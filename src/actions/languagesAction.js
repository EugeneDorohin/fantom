import axios from 'axios';
import { GET_LANGUAGES, CHOOSE_LANGUAGES, GET_LANGUAGES_FOR_ORDER } from '../actions/types';
import { GET_LANGUAGES_API, GET_CSRF_API } from '../constants/post_url';
import { getCsrf } from './csrfAction';
import { switchRegContent } from './userAction';


export const getLanguagesForOrder = languages => ({
  type: GET_LANGUAGES_FOR_ORDER,
  payload: languages
});


export const getLanguages = languages => ({
  type: GET_LANGUAGES,
  payload: languages
});

export const chooseLanguages = languages => ({
  type: CHOOSE_LANGUAGES,
  payload: languages
});


export const mapStateToProps = state => ({
  languages: state.languages.languages_list,
  choose_languages: state.languages.choose_languages,
  csrf: state.csrf.token
})

export const mapDispatchToProps = dispatch => {
  return ({
    getLanguages: (token) => axios({
      method: 'post',
      url: GET_LANGUAGES_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      dispatch(getLanguages(res.data.data));
      dispatch(getCsrf(res.data.csrf));
    }),
    chooseLanguages: (currentLangs) => {
      dispatch(chooseLanguages(currentLangs))
    },
    switchRegContent: (content) => {
      dispatch(switchRegContent(content))
    }
  })
}

export const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,  // optional
    ...dispatchProps,  // optional
    onChangeWithToken: () => {
      if (stateProps.csrf !== '') {
        const token = new FormData();
        token.append('csrf', stateProps.csrf);
        return (
          dispatchProps.getLanguages(token)
        )
      } else {
        return (
          axios.post(GET_CSRF_API).then(function (response) {
            const token2 = new FormData();
            token2.append('csrf', response.data.csrf);
            dispatchProps.getLanguages(token2)
          })
        )
      }
      
    }
  }
}