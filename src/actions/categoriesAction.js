import axios from 'axios';
import { GET_CATEGORIES, CHOOSE_CATEGORIES } from './types';
import { GET_CATEGORIES_API, GET_CSRF_API } from '../constants/post_url';
import { getCsrf } from './csrfAction';
import { switchRegContent } from './userAction';


export const getCategories = categories => ({
  type: GET_CATEGORIES,
  payload: categories
});

export const chooseCategories = languages => ({
  type: CHOOSE_CATEGORIES,
  payload: languages
});

export const mapStateToProps = state => ({
  ...state,
  categories: state.categories.categories_list,
  choose_categories: state.categories.choose_categories,
  csrf: state.csrf.token
})


export const mapDispatchToProps = dispatch => {
  return ({
    getCategories: (token) => axios({
      method: 'post',
      url: GET_CATEGORIES_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      dispatch(getCategories(res.data.data));
      dispatch(getCsrf(res.data.csrf));
    }),
    chooseCategories: (currentCategory) => {
      dispatch(chooseCategories(currentCategory))
    },
    switchRegContent: (content) => {
      dispatch(switchRegContent(content))
    }
  })
}

export const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,  // optional
    ...dispatchProps,  // optional
    onChangeWithToken: () => {
      if (stateProps.csrf !== '') {
        const token = new FormData();
        token.append('csrf', stateProps.csrf);
        return (
          dispatchProps.getCategories(token)
        )
      } else {
        return (
          axios.post(GET_CSRF_API).then(function (response) {
            const token2 = new FormData();
            token2.append('csrf', response.data.csrf);
            dispatchProps.getCategories(token2)
          })
        )
      }
      
    }
  }
}
