import axios from 'axios';
import { LOGIN, LOAD_PAGE, USER_DATA, WITHDRAW_SYSTEMS, HISTORY_LIST, SWITCH_REG_BLOCKS } from './types';
import { USER_INFO_API, GET_CSRF_API, GET_LANGUAGES_API, GET_CATEGORIES_API, GET_STATS_API } from '../constants/post_url';
import { getLanguages } from './languagesAction';
import { getCategories } from './categoriesAction';
import { getCsrf } from './csrfAction';
import { getStatic } from './staticData';
import subscribe from '../firebase_subscribe';


export const LoginAction = login => ({
  type: LOGIN,
  payload: login
});

export const getUserData = data => ({
  type: USER_DATA,
  payload: data
});

export const loadPage = data => ({
  type: LOAD_PAGE,
  payload: data
});

export const getPayment = data => ({
  type: WITHDRAW_SYSTEMS,
  payload: data
});

export const getHistory = data => ({
  type: HISTORY_LIST,
  payload: data
});

export const mapStateToProps = state => ({
  ...state,
  isLoad: state.user.isLoad
})

export const switchRegContent = content => ({
  type: SWITCH_REG_BLOCKS,
  payload: content
});

export const mapDispatchToProps = dispatch => ({
  checkUserIsLogin: axios.post(GET_CSRF_API).then(function (response) {
    const token = new FormData();
    token.append('csrf', response.data.csrf);
    
    axios({
      method: 'post',
      url: USER_INFO_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      if (res.data.error_code === 201 || res.data.error_code === -1) {
        dispatch(LoginAction(false));
      } else {
        dispatch(getUserData(res.data.data));
        dispatch(LoginAction(true));
        subscribe();
      }
      dispatch(loadPage(true));
      dispatch(getCsrf(res.data.csrf));
      token.set('csrf', res.data.csrf);
    });

    axios({
      method: 'post',
      url: GET_LANGUAGES_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      dispatch(getLanguages(res.data.data));
      dispatch(getCsrf(res.data.csrf));
      token.set('csrf', res.data.csrf);
    })

    axios({
      method: 'post',
      url: GET_CATEGORIES_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      dispatch(getCategories(res.data.data));
      dispatch(getCsrf(res.data.csrf));
      token.set('csrf', res.data.csrf);
    })

    axios({
      method: 'post',
      url: GET_STATS_API,
      data: token,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
      dispatch(getStatic(res.data.data));
      dispatch(getCsrf(res.data.csrf));
    })

  }),
  getCsrf: (csrf) => dispatch(getCsrf(csrf))
  // getLanguages: axios.post(GET_CSRF_API).then(function (response) {
    
  // }),
  // getCategories: axios.post(GET_CSRF_API).then(function (response) {
    
  // })
})