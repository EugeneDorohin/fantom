import { GET_ORDER, IS_ORDER } from '../actions/types';

export const getOrder = items => ({
  type: GET_ORDER,
  payload: items
});

export const takeAnOrder = order => ({
  type: IS_ORDER,
  payload: order
});

export const mapStateToProps = state => ({
    ...state,
    items: state.get_order.items,
    csrf: state.csrf.token
})
  
export const mapDispatchToProps = dispatch => ({
    getOrder: items => {
        dispatch(getOrder(items))
    }
})