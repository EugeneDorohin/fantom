import { GET_STATIC } from './types';


export const getStatic = staticData => ({
  type: GET_STATIC,
  payload: staticData
});