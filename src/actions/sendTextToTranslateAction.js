import { ADD_TEXT_TO_TRANSFORM,
    ADD_COUNT_TO_TRANSFORM,
    ADD_CATEGORY_TO_TRANSFORM,
    ADD_LANGUAGES_TO_TRANSFORM,
    ADD_COMMENT_TO_TRANSFORM,
    ADD_EMAIL_TO_TRANSFORM,
    ADD_TO_TRANSFORM_SWITCH_BLOCKS, 
    ADD_LANGUAGE_FROM_RANSFORM} from '../actions/types';

export const switchContent = content => ({
    type: ADD_TO_TRANSFORM_SWITCH_BLOCKS,
    payload: content
});

export const addTextToTranslate = text => ({
    type: ADD_TEXT_TO_TRANSFORM,
    payload: text
});

export const addCountToTranslate = text => ({
    type: ADD_COUNT_TO_TRANSFORM,
    payload: text
});

export const addLanguageFromTranslate = text => ({
    type: ADD_LANGUAGE_FROM_RANSFORM,
    payload: text
});

export const addCategoryToTranslate = category => ({
    type: ADD_CATEGORY_TO_TRANSFORM,
    payload: category
});

export const addLanguagesToTranslate = languages => ({
    type: ADD_LANGUAGES_TO_TRANSFORM,
    payload: languages
});

export const addCommentToTranslate = comment => ({
    type: ADD_COMMENT_TO_TRANSFORM,
    payload: comment
});

export const addEmailToTranslate = email => ({
    type: ADD_EMAIL_TO_TRANSFORM,
    payload: email
});
