import axios from 'axios';
import { GET_CSRF } from '../actions/types';
import { GET_CSRF_API } from '../constants/post_url';


export const getCsrf = csrf => ({
  type: GET_CSRF,
  payload: csrf
});

export const mapStateToProps = ({ csrf }) => ({
  csrf: csrf.token
})

export const mapDispatchToProps = dispatch => ({
  getCsrf: axios.post(GET_CSRF_API).then(function (response) {
    dispatch(getCsrf(response.data.csrf))
  })
})