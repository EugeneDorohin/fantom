import { ACTIVE_FORM_LOGIN_FROM_HEADER, MESSAGE_TEXT } from "./types";

export const openPopup = (popup, target, from = 'extra') => {
    if (target) target.preventDefault();

    document.body.classList.add('no_scroll');
    document.querySelector('.popup_wrapper[data-popup="' + popup + '"]').classList.add('open_popup', from);
}

export const openPopupSimple = popup => {
    document.body.classList.add('no_scroll');
    document.querySelector('.popup_wrapper[data-popup="' + popup + '"]').classList.add('open_popup');
}

export const closePopup = (event) => {
    if (event) event.preventDefault();
    document.body.classList.remove('no_scroll');
    [...document.querySelectorAll('.popup_wrapper')].map(item => {
        item.classList.remove('open_popup')
    });
}



export const addMessageText = text => ({
    type: MESSAGE_TEXT,
    payload: text
  });


export const activeFromHeader = data => ({
    type: ACTIVE_FORM_LOGIN_FROM_HEADER,
    payload: data
  });