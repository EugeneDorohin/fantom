import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { mapDispatchToProps, mapStateToProps } from '../actions/csrfAction'

import App from '../components/App';

export default App;
