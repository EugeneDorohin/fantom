// const BASE_URL = '/08_16_translate/front/'
const BASE_URL = '/'

export const HOME = BASE_URL;
export const TRANSLATOR = BASE_URL + 'translator';
export const WORK = BASE_URL + 'work';
export const BALANCE = BASE_URL + 'balance';
export const ABOUT = BASE_URL + 'about';
export const HELP = BASE_URL + 'help';
export const RULES = BASE_URL + 'rules';
export const CONTACT = BASE_URL + 'contact';
export const CHAT = BASE_URL + 'chat';

export const STATUS = BASE_URL + 'order-status';
export const FAIL_PAY = BASE_URL + 'payment-failing';