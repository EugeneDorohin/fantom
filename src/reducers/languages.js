import { GET_LANGUAGES, CHOOSE_LANGUAGES, GET_LANGUAGES_FOR_ORDER } from '../actions/types';


const initialState = {
    languages_list: [],
    languages_list_for_order: [],
    choose_languages: {}
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LANGUAGES:
            return {
                ...state,
                languages_list: action.payload
            }
        case GET_LANGUAGES_FOR_ORDER:
            return {
                ...state,
                languages_list_for_order: action.payload
            }
        case CHOOSE_LANGUAGES:
            return {
                ...state,
                choose_languages: action.payload
            }
        default:
            return state;
    }
};
