import { GET_STATIC } from '../actions/types';


const initialState = {
    staticData: null
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_STATIC:
            return {
                ...state,
                staticData: action.payload
            }
        default:
            return state;
    }
};
