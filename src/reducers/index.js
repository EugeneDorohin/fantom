import { combineReducers } from 'redux';

import csrf from './csrf';
import user from './user';
import languages from './languages';
import categories from './categories';
import get_order from './get_order';
import sendTextToTranslate from './sendTextToTranslate';
import form from './form';
import staticData from './staticData';

export default combineReducers({
  csrf,
  user,
  languages,
  categories,
  get_order,
  sendTextToTranslate,
  form,
  staticData
});
