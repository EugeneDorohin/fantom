import { GET_ORDER, IS_ORDER } from '../actions/types';


const initialState = {
    items: {},
    isOrder: false
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ORDER:
            return {
                ...state,
                items: action.payload
            }
        case IS_ORDER:
            return {
                ...state,
                isOrder: action.payload
            }
        default:
            return state;
    }
};
