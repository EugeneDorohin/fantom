import { ADD_TEXT_TO_TRANSFORM,
         ADD_COUNT_TO_TRANSFORM,
         ADD_CATEGORY_TO_TRANSFORM,
         ADD_LANGUAGES_TO_TRANSFORM,
         ADD_COMMENT_TO_TRANSFORM,
         ADD_EMAIL_TO_TRANSFORM,
         ADD_TO_TRANSFORM_SWITCH_BLOCKS, 
         ADD_LANGUAGE_FROM_RANSFORM} from '../actions/types';


const initialState = {
    contentSwitch: 'text',
    email: '',
    category: {},
    language_id: [],
    text: '',
    count: '',
    language_from: '',
    comment: ''
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_TRANSFORM_SWITCH_BLOCKS:
            return {
                ...state,
                contentSwitch: action.payload
            }
        case ADD_TEXT_TO_TRANSFORM:
            return {
                ...state,
                text: action.payload
            }
        case ADD_COUNT_TO_TRANSFORM:
            return {
                ...state,
                count: action.payload
            }
        case ADD_LANGUAGE_FROM_RANSFORM:
            return {
                ...state,
                language_from: action.payload
            }
        case ADD_CATEGORY_TO_TRANSFORM:
            return {
                ...state,
                category: action.payload
            }
        case ADD_LANGUAGES_TO_TRANSFORM:
            return {
                ...state,
                language_id: action.payload
            }
        case ADD_COMMENT_TO_TRANSFORM:
            return {
                ...state,
                comment: action.payload
            }
        case ADD_EMAIL_TO_TRANSFORM:
            return {
                ...state,
                email: action.payload
            }
        default:
            return state;
    }
};
