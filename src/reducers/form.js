import { ACTIVE_FORM_LOGIN_FROM_HEADER, MESSAGE_TEXT } from '../actions/types';


const initialState = {
    fromHeader: false,
    messageText: ''
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case ACTIVE_FORM_LOGIN_FROM_HEADER:
            return {
                ...state,
                fromHeader: action.payload
            }
        case MESSAGE_TEXT:
            return {
                ...state,
                messageText: action.payload
            }
        default:
            return state;
    }
};
