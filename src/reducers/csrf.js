import { GET_CSRF } from '../actions/types';


const initialState = {
    token: ''
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CSRF:
            return {
                ...state,
                token: action.payload
            }
        default:
            return state;
    }
};
