import { GET_CATEGORIES, CHOOSE_CATEGORIES } from "../actions/types";

const initialState = {
    categories_list: [],
    choose_categories: {}
};
  
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES:
            return {
                ...state,
                categories_list: action.payload
            }
        case CHOOSE_CATEGORIES:
            return {
                ...state,
                choose_categories: action.payload
            }
        default:
            return state;
    }
};
