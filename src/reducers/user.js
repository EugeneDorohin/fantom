import { LOGIN, LOAD_PAGE, USER_DATA, WITHDRAW_SYSTEMS, HISTORY_LIST, SWITCH_REG_BLOCKS } from "../actions/types";

const initialState = {
    userData: {},
    isLoggedIn: false,
    isLoad: false,
    withdraw_systems: null,
    history_list: [],
    contentRegBlock: true
}
export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                isLoggedIn: action.payload
            }
        case USER_DATA:
            return {
                ...state,
                userData: action.payload
            }
        case LOAD_PAGE:
            return {
                ...state,
                isLoad: action.payload
            }
        case WITHDRAW_SYSTEMS:
            return {
                ...state,
                withdraw_systems: action.payload
            }
        case HISTORY_LIST:
            return {
                ...state,
                history_list: action.payload
            }
        case SWITCH_REG_BLOCKS:
            return {
                ...state,
                contentRegBlock: action.payload
            }
        default:
            return state;
    }
};
