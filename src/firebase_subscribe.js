import $ from 'jquery';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/messaging';

// firebase_subscribe.js
firebase.initializeApp({
    messagingSenderId: '621516967658'//ID приложения firebase
});

// браузер поддерживает уведомления
if ('Notification' in window) {
    try {     
        var messaging = firebase.messaging();
      
    } catch (err) {
        // console.log(err);
    }
}

export default function subscribe() {
    // запрашиваем разрешение на получение уведомлений
    try {
        messaging.requestPermission()
            .then(function () {
                // получаем ID устройства
                messaging.getToken()
                    .then(function (currentToken) {
                        //console.log(currentToken);

                        if (currentToken) {
                            sendTokenToServer(currentToken);
                        } else {
                            //console.warn('Не удалось получить токен.');
                            setTokenSentToServer(false);
                        }
                    })
                    .catch(function (err) {
                        // console.warn('При получении токена произошла ошибка.', err);
                        setTokenSentToServer(false);
                    });
        })
        .catch(function (err) {
            // console.warn('Не удалось получить разрешение на показ уведомлений.', err);
        });
    } catch (err) {
        // console.log(err);
    }
}

// отправка ID на сервер
function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer(currentToken)) {
        //console.log('Отправка токена на сервер...');

        var url = 'save_push_token.php'; // адрес скрипта на сервере который сохраняет ID устройства
        $.getJSON(url, {token: currentToken}, function (data) {
            if (data.result == "Ok")
                setTokenSentToServer(currentToken);
        });
    } else {
        //console.log('Токен уже отправлен на сервер.');
    }
}

// используем localStorage для отметки того,
// что пользователь уже подписался на уведомления
function isTokenSentToServer(currentToken) {
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currentToken;
}

function setTokenSentToServer(currentToken) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currentToken ? currentToken : ''
    );
}

try {
    // handle catch the notification on current page
    messaging.onMessage(function(payload) {
        console.log('Message received. ', payload);

        // регистрируем пустой ServiceWorker каждый раз
        navigator.serviceWorker.register('firebase-messaging-sw.js');

        // запрашиваем права на показ уведомлений если еще не получили их
        Notification.requestPermission(function(result) {
            if (result === 'granted') {
                navigator.serviceWorker.ready.then(function(registration) {
                    // своя логика как в примере с TTL и т.д.

                    // копируем объект data
                    payload.data.data = JSON.parse(JSON.stringify(payload.data));

                    registration.showNotification(payload.data.title, payload.data);
                }).catch(function(error) {
                    console.log('ServiceWorker registration failed', error);
                });
            }
        });
    });
} catch (err) {

}