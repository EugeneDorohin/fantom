import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';
import { NavLink } from 'react-router-dom';

import soundfile from '../../sounds/translate-done.mp3';

import HeaderStatus from '../partials/HeaderStatus';
import { GET_CSRF_API, ORDER_INFO_API } from '../../constants/post_url';
import { getCsrf } from '../../actions/csrfAction';
import BadTranslate from './partials/form/BadTranslate';
import { openPopupSimple } from '../../actions/form';

import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

class Status extends Component {

    constructor(props) {
        super(props);
        let intervalTimeSearch = null;
        let intervalTimeWork = null;
        this.state = {
            times: null,
            error: false,
            status: null,
            hash: null,
            wait_time: 0,
            translationProgress: 0,
            translatorProgress: 0
        }
    }


    componentDidMount () {
        ReactGA.pageview(routes.STATUS);
        if (this.intervalTimeSearch) clearInterval(this.intervalTimeSearch);
        this.intervalTimeSearch = setInterval (()=>{
            if (['1','2','5','6'].includes(this.state.status)) {
                this.setState({
                    wait_time: this.state.wait_time + 1,
                    translationProgress: this.state.translationProgress < 100 ? Math.floor(100 * (this.state.wait_time - this.state.times.wait_translator) / this.state.times.wait_translation) : 100,
                    translatorProgress: this.state.translationProgress < 100 ? Math.floor(100 * this.state.wait_time / this.state.times.wait_translator) : 100,
                })
            }
        }, 1000);
    }

    componentWillMount () {
        this.orderStatus();
    }

    orderStatus = () => {
        const getUrl = window.location.href;
        const urlObj = new URL(getUrl);
        const hash = urlObj.searchParams.get('hash');

        if (!hash) {
            this.setState({
                status: '20'
            });
        }
        
        const self = this;
        axios.post(GET_CSRF_API).then(function (res) {
            const listForm = new FormData();
            listForm.append('csrf', res.data.csrf);
            listForm.append('hash', hash);
            axios({
                method: 'post',
                url: ORDER_INFO_API,
                data: listForm,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            }).then(function (res) {
                self.props.getCsrf(res.data.csrf);
                if (res.data.error_code === -1) {
                    self.orderStatus();
                }
                if (res.data.error_code === 401) {
                    self.setState({
                        status: '20'
                    });
                } else {
                    self.setState({
                        times: res.data.data,
                        status: res.data.data.status,
                        hash: hash,
                        wait_time: res.data.data.wait_time
                    });
                    setTimeout(()=>{
                        self.orderStatus();
                    },20000);
                }
            }) 
        })
    }

    singlePush = () => {
        this.myRef = React.createRef();
        return (
            <audio ref={this.myRef} src={soundfile} autoPlay/>
        )
    }

    badTranslate = () => {
        openPopupSimple('badTranslate');
    }

    render () {

        const { status, hash, times } = this.state;
        let textHeader, textMessage;

        if (times == null && status != '20') {
            return false
        }

        switch(status) {
            default:
                
                return (
                    <div className="wrapper_page">
                        <HeaderStatus text='Ошибка' history={this.props.history} />
                        <main>
                            <div className="page">
                                <div className="info_block">
                                    <div className="ico"></div>
                                    <p>
                                        Invalid hash
                                    </p>
                                </div>

                                <div className="wrapper_button_work">
                                    <NavLink to={routes.HOME} className="btn">New Translate</NavLink>
                                </div>
                            </div>
                        </main>
                    </div>
                )
            case '0':
            case '3':
            case '9':
                switch(status) {
                    case '0':
                        textHeader = 'Waiting for payment';
                        textMessage = 'As soon as payment is received, we will proceed to translation';
                        break;
                    case '3':
                        textHeader = 'Waiting for evaluation';
                        textMessage = 'Translation is pending';
                        break;
                    case '9':
                        textHeader = 'Deleted';
                        textMessage = 'There is no more translation :(';
                        break;
                    default:
                        break;
                }
                return (
                    <div className="wrapper_page">
                        <HeaderStatus text={textHeader} />
                        <main>
                            <div className="page">
                                <div className="info_block">
                                    {status == '9' ? <div className="ico time"></div> : <svg className="loader" width="44" height="44" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>}
                                    <p>
                                        {textMessage}
                                    </p>
                                </div>

                                <div className="wrapper_button_work">
                                    <NavLink to={routes.HOME} className="btn">New translate</NavLink>
                                </div>
                            </div>
                        </main>
                    </div>
                )
            case '1':
            case '2':
            case '5':
            case '6':
                const {left_time, wait_translation, wait_translator} = this.state.times;
                const translatorProgress = this.state.translatorProgress;
                const translationProgress = this.state.translationProgress < 0 ? 0 : this.state.translationProgress;
                return (
                    <div className="wrapper_page">
                        <HeaderStatus left_time={'~'+(Math.floor(wait_translator / 60) + Math.floor(wait_translation / 60))+' Minutes'} />
                        <main>
                            <div className="page status_search">
                                
                                <div className="items">
                                    <div className="item">
                                        <div className="progress_wrapper">
                                            <CircularProgressbar
                                                className='progress_bar'
                                                percentage={translatorProgress}
                                                strokeWidth='10'
                                                styles={{
                                                    path: { stroke: `#29ABE2, ${translatorProgress / 100})`, strokeLinecap: 'butt' },
                                                    trail: { stroke: 'transparent' },
                                                    strokeLinecap: 'butt',
                                                }}
                                                initialAnimation={true}
                                            />
                                        </div>
                                        <div className="t">Search translator</div>
                                        <div className="time">~{Math.floor(wait_translator / 60)} Minutes</div>
                                    </div>
                                    <div className="item">
                                        <div className="progress_wrapper">
                                            <CircularProgressbar
                                                className='progress_bar'
                                                percentage={translationProgress}
                                                strokeWidth='10'
                                                styles={{
                                                    path: { stroke: `#29ABE2, ${translationProgress / 100})`, strokeLinecap: 'butt' },
                                                    trail: { stroke: 'transparent' },
                                                }}
                                                initialAnimation={true}
                                            />
                                        </div>
                                        <div className="t">Translate</div>
                                        <div className="time">~{Math.floor(wait_translation / 60)} Minutes</div>
                                    </div>
                                </div>
        
                                <NavLink to={routes.WORK} className="btn">New translate</NavLink>
        
                            </div>
                        </main>
                    </div>
                )
            case '4':
                textHeader = 'Translation is done';
                let nameTranslator = this.state.times.translator;
                
                const result = this.state.times.result_translation && this.state.times.result_translation.map(item => (
                    <a href={item.url} download title={item.type}><img src={item.logo} alt={item.type} width="70" height="70" /></a>
                ))
                const result_editing = this.state.times.result_editing && this.state.times.result_editing.map(item => (
                    <a href={item.url} download title={item.type}><img src={item.logo} alt={item.type} width="70" height="70" /></a>
                ))
                return (
                    <div className="wrapper_page">
                        <HeaderStatus text={textHeader} />
                        <main>
                            <div className="page">
                                
                                <div className="order_accept">
                                    <div className="translator">
                                        <div className="ico_smile"></div>
                                        <div>{nameTranslator}</div>
                                    </div>
                                    <button className="btn btn_red" onClick={this.badTranslate}>Bad translate</button>
                                    
                                    <div className={'result_wrapper' + (!this.state.times.result_translation ? ' hidden' : '')}>
                                        {result}
                                    </div>
                                    
                                    <div className={'result_editing' + (!this.state.times.result_editing ? ' hidden' : '')}>
                                        <div className="title">Corrected</div>
                                        <div className="result_wrapper">
                                            {result_editing}
                                        </div>
                                    </div>
                    
                                    <div>
                                        <NavLink to={routes.HOME} className="btn">New translate</NavLink>
                                    </div>

                                </div>
                                
                            </div>
                        </main>
                        <BadTranslate hash={hash} history={this.props.history} />
                        {this.singlePush()}
                    </div>
                )
        }
    }
}

const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
});

export const mapDispatchToProps = dispatch => ({
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
});

export default connect(mapStateToProps, mapDispatchToProps)(Status);
// export default Status;