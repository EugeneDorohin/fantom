import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

import HeaderMain from '../partials/HeaderMain';
import TabsLink from './partials/TabsLink';
import Categories from './partials/Categories';
import Languages from './partials/Languages';
import AboutText from './partials/AboutText';
import FormAddToTranslate from './partials/form/index';

class Translate extends Component {

    componentDidMount () {
        if (this.props.user.isLoggedIn === false) {
            ReactGA.pageview(routes.BALANCE);
        }
    }

    switchContent = (contentSwitch) => {
        switch (contentSwitch) {
            case 'text':
                return <TabsLink />
            case 'category':
                return <Categories />
            case 'languages':
                return <Languages />
            default:
                break;
        }
    }

    render () {
        const contentSwitch = this.switchContent(this.props.contentSwitch);
        if (this.props.user.isLoggedIn === false) {
            return (
                <div className="wrapper_page">
                    <HeaderMain history={this.props.history} />
                    <main>
                        { contentSwitch }
                        <AboutText />
                        <FormAddToTranslate />
                    </main>
                </div>
            );
        }
        else {
            return (
                <Redirect to={routes.WORK} />
            )
        }
    }
}

const mapStateToProps = state => ({
    ...state,
    contentSwitch: state.sendTextToTranslate.contentSwitch
})


export default connect(mapStateToProps)(Translate);