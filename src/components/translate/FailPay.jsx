import React, { Component } from 'react';
import * as routes from '../../constants/routes';
import { NavLink } from 'react-router-dom';
import HeaderStatus from '../partials/HeaderStatus';

class FailPay extends Component {

    

    render () {
        return (
            <div className="wrapper_page">
                <HeaderStatus text="Payment failing" />
                <main>
                    <div className="page">
                        <div className="info_block">
                            <svg className="loader" width="44" height="44" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>
                            <p>
                                At the moment, your payment for some reason has not yet been received. Please check what’s wrong with it in your payment system or bank account. Once it is received, we will immediately start performing the order. Thank you.
                            </p>
                        </div>

                        <div className="wrapper_button_work">
                            <NavLink to={routes.HOME} className="btn">New translate</NavLink>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}


export default FailPay;