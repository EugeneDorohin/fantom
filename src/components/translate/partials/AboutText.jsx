import React, { Component } from 'react';
import { connect } from 'react-redux';

class AboutText extends Component {

    
    render () {
        return (
            <div>
                <section className="how_it_work">
                    <div className="page">
                        <div className="title_inside">How it works</div>
                        <div className="steps">
                        <div className="step">
                            <div className="n">1</div>
                            <div className="text">Add the text or files</div>
                        </div>
                        <div className="step">
                            <div className="n">2</div>
                            <div className="text">Define the category</div>
                        </div>
                        <div className="step">
                            <div className="n">3</div>
                            <div className="text">Define the translation languages</div>
                        </div>
                        <div className="step">
                            <div className="n">4</div>
                            <div className="text">Submit your e-mail to receive the result</div>
                        </div>
                        <div className="step">
                            <div className="n">5</div>
                            <div className="text">Make a payment</div>
                        </div>
                        <div className="step">
                            <div className="n">6</div>
                            <div className="text">Receive your translation</div>
                        </div>
                    </div>
                    </div>
                </section>
                <div className="page">
                    <section className="why">
                        <div className="title_inside">Why it is cool</div>
                        <div className="items">
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/speedometer.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Speed</div>
                                    <p>
                                        We have free registration of translators here. Their big amount creates conditions for quick reaction on each translation order, which minimizes the time.
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/dollar-symbol.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Price</div>
                                    <p>
                                        The price depends on market conditions of supply and demand, which creates a completely fair market price, dragging it down to a possible minimum.
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/quality.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Quality</div>
                                    <p>
                                        Your order will be done by the best possible translator from the available ones. If you order the quality control option, the translation will be verified and edited by the best performers.
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/security.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Safety</div>
                                    <p>
                                        If the received translation is incorrect, you can send it back to make corrections with their description and receive a new one, corrected, in a short time.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <section className="info_numbers">
                    <div className="page">
                        <div className="items">
                            <div className="item">
                                <div className="n">{this.props.staticData && Math.floor(this.props.staticData.wait_time / 60)} Minutes</div>
                                <div className="text">Is the average speed</div>
                            </div>
                            <div className="item">
                                <div className="n">{this.props.staticData && this.props.staticData.total_orders}</div>
                                <div className="text">Orders</div>
                            </div>
                            <div className="item">
                                <div className="n">{this.props.staticData && Math.floor(this.props.staticData.total_symbols / 5)}</div>
                                <div className="text">Words translated</div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="page">
                    <section className="our_mission">
                        <div className="title_inside">Our goal</div>
                        <div className="text">
                            Your order will be done by the best possible translator from the available ones. If you order the quality control option, the translation will be verified and edited by the best performers.
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    staticData: state.staticData.staticData
})

export default connect(mapStateToProps)(AboutText);