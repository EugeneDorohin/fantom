import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as routes from '../../../constants/routes';
import { getCsrf } from '../../../actions/csrfAction';
import { switchContent, addLanguagesToTranslate } from '../../../actions/sendTextToTranslateAction';
import { GET_LANGUAGES_API, GET_CSRF_API } from '../../../constants/post_url';
import { openPopup } from '../../../actions/form';

class Languages extends Component {

    state = {
        // languages: [],
        items: ''
    }
    
    back = () => {
        this.props.switchContent('category');
    }

    next = () => {
        this.props.addLanguagesToTranslate(this.state.items);
        openPopup('addToTranslate');
    }

    currentLang = (event) => {
        let target = event.currentTarget;
        
        [...document.querySelectorAll('.item_lang_add')].filter(item => {
            item.classList.remove('current');
        });
        this.setState({
            items: target.getAttribute('data-key')
        });
        target.classList.add('current');
    }

    // componentWillMount () {
    //     const self = this;
    //     const listForm = new FormData();
    //     listForm.append('csrf', this.props.csrf);
    //     axios({
    //         method: 'post',
    //         url: GET_LANGUAGES_API,
    //         data: listForm,
    //         config: { headers: {'Content-Type': 'multipart/form-data' }}
    //     }).then(function (res) {
    //         console.log(res);
    //         self.setState({
    //             languages: res.data.data
    //         })
    //     });
    // }

    render () {
        // const { price_mn } = this.props.sendTextToTranslate.category;
        const itemsLanguages = this.props.languages.languages_list_for_order.map(lang => (
            <div key={lang.id} data-key={lang.id} className="item item_lang_add" onClick={this.currentLang}>
                <img className="image" src={lang.logo} alt={lang.name} width="38" height="38" />
                <div className="text">
                    <p>
                        <div>{lang.name}</div>
                        { lang.price_start != lang.price_end ?
                            <span className="old">${ lang.price_start }</span>
                            : ''
                        }
                        <span>${ lang.price_end }</span>
                    </p>
                </div>
            </div>
        ));
        return (
            <div className="page wrapper_choose">
                <div className="title_page">
                    <button className="back" onClick={this.back}>
                        <svg width="38" height="38" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 400.004 400.004">
                            <path d="M382.688,182.686H59.116l77.209-77.214c6.764-6.76,6.764-17.726,0-24.485c-6.764-6.764-17.73-6.764-24.484,0L5.073,187.757 c-6.764,6.76-6.764,17.727,0,24.485l106.768,106.775c3.381,3.383,7.812,5.072,12.242,5.072c4.43,0,8.861-1.689,12.242-5.072 c6.764-6.76,6.764-17.726,0-24.484l-77.209-77.218h323.572c9.562,0,17.316-7.753,17.316-17.315 C400.004,190.438,392.251,182.686,382.688,182.686z"/>
                        </svg>
                    </button>
                    Language
                </div>
                <p className="first_text_free">First 500 characters come for free!</p>
                <div className="items_choose">
                    {itemsLanguages}
                </div>
                <div className="button_choose_wrapper">
                    <button  className="btn btn_blue button_choose" onClick={this.next} disabled={!this.state.items.length}>Select language</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
    csrf: state.csrf.token
})
  
  
const mapDispatchToProps = dispatch => {
    return ({
        switchContent: (content) => {
            dispatch(switchContent(content))
        },
        addLanguagesToTranslate: languages => {
            dispatch(addLanguagesToTranslate(languages))
        },
        // getCsrf: axios.post(GET_CSRF_API).then(function (res) {
        //     dispatch(getCsrf(res.data.csrf))
        // })
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Languages);