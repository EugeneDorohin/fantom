import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import * as routes from '../../../../constants/routes';
import { closePopup, openPopup, openPopupSimple, addMessageText } from '../../../../actions/form';
import { ORDER_CANCEL_API } from '../../../../constants/post_url';
import { getCsrf } from '../../../../actions/csrfAction';

class BadTranslate extends Component {
    
    state = {
        text: ''
    }

    commentChange = (e) => {
        this.setState({
            text: e.currentTarget.value
        });
    }

    sendProblem = () => {
        if (this.state.text !== '') {
            const listForm = new FormData();
            listForm.append('csrf', this.props.csrf);
            listForm.append('hash', this.props.hash);
            listForm.append('comment', this.state.text);
            const self = this;
            axios({
                method: 'post',
                url: ORDER_CANCEL_API,
                data: listForm,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            }).then(function (res) {
                self.props.getCsrf(res.data.csrf);
                if (res.data.error_code === 401) {
                    closePopup();
                    // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else if (res.data.error_code === 403) {
                    closePopup();
                    // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else {
                    closePopup();
                    this.props.history.push(routes.STATUS + '?hash=' + self.props.hash);
                    window.scrollTo(0, 0);
                }
            }) 
        }
    }

    render () {
        return (
            <div className="popup_wrapper" data-popup="badTranslate">
                <div className="bg" onClick={closePopup}></div>
                <div className="popup">
                    <div className="close">
                        <button onClick={closePopup}>
                            <svg width="19" height="19" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                            </svg>
                        </button>
                    </div>
                    <div className="content_popup">
                        <div className="title">Problem description</div>
                        <div className="field">
                            <textarea value={this.state.text} placeholder="Describe the problem of translation" onChange={this.commentChange}></textarea>
                        </div>
                        <button className="btn btn_blue comment_btn" onClick={this.sendProblem} disabled={!this.state.text}>Send</button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    csrf: state.csrf.token
});

const mapDispatchToProps = dispatch => ({
    addMessageText: (text) => dispatch(addMessageText(text)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})

export default connect(mapStateToProps, mapDispatchToProps)(BadTranslate);