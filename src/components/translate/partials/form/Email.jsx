import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import * as routes from '../../../../constants/routes';
import { addEmailToTranslate } from '../../../../actions/sendTextToTranslateAction';
import { GET_CSRF_API, ADD_ORDER_API } from '../../../../constants/post_url';
import { getCsrf } from '../../../../actions/csrfAction';
import { addMessageText, openPopupSimple } from '../../../../actions/form';

class AddEmail extends Component {

    state = {
        email: '',
        error: true,
        isError: false,
        errorText: '',
        loadBtn: false
    }
    
    next = () => {
        this.props.addEmailToTranslate(this.state.email);
        const self = this;
        const { text, count, category, language_id, comment} = this.props.sendTextToTranslate;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('email', this.state.email);
        listForm.append('category_id', category.id );
        listForm.append('language_id', language_id);
        listForm.append('text', text);
        listForm.append('comment', comment);
        this.setState({
            loadBtn: true
        });
        axios({
            method: 'post',
            url: ADD_ORDER_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 101) {
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
                this.setState({
                    loadBtn: false
                });
            } else if (res.data.error_code === 301) {
                self.props.addMessageText(res.data.text);
                // self.props.addMessageText('Не удалось добавить заказ, проверьте вводные данные');
                openPopupSimple('messagePopup');
                this.setState({
                    loadBtn: false
                });
            } else {
                if (res.data.data.pay_url) {
                    window.location.href = res.data.data.pay_url;
                } else {
                    window.location.href = routes.STATUS + '?hash=' + res.data.data.hash;
                }
            }
        });
    }

    emailChange = (e) => {
        let input = e.currentTarget,
            reg = /^.+@.+\..+$/i;
        if ( !reg.test(input.value) ) {
            input.parentNode.classList.add('error');
            this.setState({
                error: true
            });
        } else {
            input.parentNode.classList.remove('error');
            this.setState({
                error: false,
                isError: false
            });
        }
        this.setState({
            email: e.currentTarget.value
        });
    }
    
    render () {
        return (
            <div>
                <div className="title">Data to receive</div>
                <div className="field">
                    <input type="text" placeholder="E-mail" value={this.state.email} onChange={this.emailChange} />
                </div>
                
                <div className="wrapper_buttons error_wrapper">
                    <button className={"btn btn_blue email_btn" + (this.state.loadBtn ? ' load_btn': '')} onClick={this.next} disabled={this.state.error || this.state.loadBtn}>
                        <svg className="loader" width="44" height="44" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>
                        <span>Save</span>
                    </button>
                    <p className={this.state.isError ? "error_text active" : "error_text"} >{this.state.errorText}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
})
  
  
const mapDispatchToProps = dispatch => {
    return ({
        addEmailToTranslate: email => {
            dispatch(addEmailToTranslate(email))
        },
        getCsrf: (csrf) => dispatch(getCsrf(csrf)),
        addMessageText: (text) => dispatch(addMessageText(text)),
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEmail);