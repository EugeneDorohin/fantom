import React, { Component } from 'react';
import Email from './Email';
import Comment from './Comment';

class FormsFromAddToTranslate extends Component {
    
    state = {
        formContentSwitch: 'comment'
    }

    closePopup = (event) => {
        event.preventDefault();
        document.body.classList.remove('no_scroll');
        [...document.querySelectorAll('.popup_wrapper')].map(item => {
            return (
                item.classList.remove('open_popup')
            )
        });
        setTimeout(()=> {
            this.changeContentState('comment');
        }, 501);
    }

    changeContent = () => {
        switch (this.state.formContentSwitch) {
            case 'email':
                return <Email changeContent={this.changeContentState} />
            default:
                return <Comment changeContent={this.changeContentState} />
        }
    }

    changeContentState = (newFormContentSwitch) => {
        this.setState({
            formContentSwitch: newFormContentSwitch
        })
    }
    
    render () {
        return (
            <div className="popup_wrapper" data-popup="addToTranslate">
                <div className="bg" onClick={this.closePopup}></div>
                <div className="popup">
                    <div className="close">
                        <button onClick={this.closePopup}>
                            <svg width="19" height="19" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                            </svg>
                        </button>
                    </div>
                    <div className="content_popup">
                        {this.changeContent()}
                    </div>
                </div>
            </div>
        );
    }
}

export default FormsFromAddToTranslate;