import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCommentToTranslate } from '../../../../actions/sendTextToTranslateAction';

class AddComment extends Component {

    state = {
        comment: ''
    }
    
    next = () => {
        this.props.addCommentToTranslate(this.state.comment);
        this.props.changeContent('email');
    }

    commentChange = (e) => {
        this.setState({
            comment: e.currentTarget.value
        });
    }
    
    render () {
        return (
            <div>
                <div className="title">A comment to the order</div>
                <div className="field">
                    <textarea value={this.state.comment} placeholder="For example: I want a formal language" onChange={this.commentChange}></textarea>
                </div>
                <button className="btn btn_blue comment_btn" onClick={this.next}>{!this.state.comment ? 'Skip' : 'Continue'}</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
})
  
  
const mapDispatchToProps = dispatch => {
    return ({
        addCommentToTranslate: comment => {
            dispatch(addCommentToTranslate(comment))
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(AddComment);