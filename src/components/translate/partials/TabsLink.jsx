import React, { Component } from 'react';

import TabsText from './TabsText';
import TabsFiles from './TabsFiles';


class TabsLink extends Component {

    state = {
        text: true,
        files: false
    }

    changeTab = event => {
        event.preventDefault();
        let target = event.target;
        if (target.getAttribute('data-tab') === 'text') {
            this.setState({
                text: true,
                files: false
            })
        }
        if (target.getAttribute('data-tab') === 'files') {
            this.setState({
                text: false,
                files: true
            })
        }
    }
    
    render () {
        
        const { text, files } = this.state;
        let tabCurrentBlock = text === true ? <TabsText /> : <TabsFiles /> ;

        return (
            <div className="page">
                <section className="tabs">
                    <a href="/" className={text ? 'current' : ''} onClick={this.changeTab} data-tab="text">Text</a>
                    <a href="/" className={files ? 'current' : ''} onClick={this.changeTab} data-tab="files" >Files</a>
                </section>
            
                { tabCurrentBlock }
                
            </div>
        );
    }
}

export default TabsLink;