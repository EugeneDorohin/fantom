import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as routes from '../../../constants/routes';
import { getCsrf } from '../../../actions/csrfAction';
import { switchContent, addCategoryToTranslate } from '../../../actions/sendTextToTranslateAction';
import { GET_CATEGORIES_API, GET_CSRF_API, GET_LANGUAGES_FOR_ORDER_API } from '../../../constants/post_url';
import { getLanguagesForOrder } from '../../../actions/languagesAction';

class Categories extends Component {

    back = () => {
        this.props.switchContent('text');
    }

    next = (event) => {
        window.scrollTo(0, 0);
        const chooseCategory = {
            id: event.currentTarget.getAttribute('data-id'),
            price_mn: event.currentTarget.getAttribute('data-price'),
        }
        this.props.addCategoryToTranslate(chooseCategory);
        this.get_languages_for_order(chooseCategory.id);
    }

    get_languages_for_order = (category_id) => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('category_id', category_id);
        listForm.append('language_id', this.props.sendTextToTranslate.language_from);
        listForm.append('count', this.props.sendTextToTranslate.count);
        
        axios({
            method: 'post',
            url: GET_LANGUAGES_FOR_ORDER_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            self.props.getLanguagesForOrder(res.data.data);
            self.props.switchContent('languages');
        })
    }

    render () {
        const itemsCategories = this.props.categories.categories_list.map(item => (
            <div key={item.id} data-id={item.id} data-price={item.price_mn} className="item item_category" onClick={this.next}>
                <img className="image" src={item.logo} alt={item.name} width="38" height="38" />
                <div className="text">{item.name}</div>
            </div>
        ));
        return (
            <div className="page wrapper_choose">
                <div className="title_page">
                    <button className="back" onClick={this.back}>
                        <svg width="38" height="38" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 400.004 400.004">
                            <path d="M382.688,182.686H59.116l77.209-77.214c6.764-6.76,6.764-17.726,0-24.485c-6.764-6.764-17.73-6.764-24.484,0L5.073,187.757 c-6.764,6.76-6.764,17.727,0,24.485l106.768,106.775c3.381,3.383,7.812,5.072,12.242,5.072c4.43,0,8.861-1.689,12.242-5.072 c6.764-6.76,6.764-17.726,0-24.484l-77.209-77.218h323.572c9.562,0,17.316-7.753,17.316-17.315 C400.004,190.438,392.251,182.686,382.688,182.686z"/>
                        </svg>
                    </button>
                    Categories
                </div>
                <p className="first_text_free">First 500 characters come for free!</p>
                <div className="items_choose">
                    {itemsCategories}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
    csrf: state.csrf.token
})
  
  
const mapDispatchToProps = dispatch => {
    return ({
        switchContent: (content) => {
            dispatch(switchContent(content))
        },
        addCategoryToTranslate: category => {
            dispatch(addCategoryToTranslate(category))
        },
        getLanguagesForOrder: languages => {
            dispatch(getLanguagesForOrder(languages))
        },
        getCsrf: (csrf) => dispatch(getCsrf(csrf))
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);