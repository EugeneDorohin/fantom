import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { addTextToTranslate, addCountToTranslate, switchContent, addLanguageFromTranslate } from '../../../actions/sendTextToTranslateAction';
import { CHECK_FILE_API } from '../../../constants/post_url';
import { addMessageText, openPopupSimple } from '../../../actions/form';
import { getCsrf } from '../../../actions/csrfAction';

class TabsFiles extends Component {

    state = {
        addedFile: false,
        isLoadFile: false,
        file: '',
        text: '',
        name: '',
        count: '',
        countText: '',
        lang: {
            logo: '',
            name: '',
            id: ''
        }
    }

    sendText = () => {
        window.scrollTo(0, 0);
        this.props.addTextToTranslate(this.state.text);
        this.props.addCountToTranslate(this.state.count);
        this.props.addLanguageFromTranslate(this.state.lang.id);
        this.props.switchContent('category');
    }

    handleFile = (event) => {
        let val = event.currentTarget.files[0];
        
        this.setState({
            isLoadFile: true,
            file: val,
            name: val.name ? val.name : ''
        })
        this.defineLanguage(val);

    }

    handleText = (e) => {
        this.setState({
            text: e.currentTarget.value,
        })
    }

    cancelFile = () => {
        this.setState({
            isLoadFile: false,
            addedFile: false,
            countText: '',
            lang: {
                logo: '',
                name: '',
                id: ''
            },
            text: '',
            count: ''
        });
    }

    defineLanguage = (file) => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('file', file);
        
        axios({
            method: 'post',
            url: CHECK_FILE_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 301) {
                self.props.addMessageText(res.data.text);
                // self.props.addMessageText('Не удалось определить текст/язык в файле');
                openPopupSimple('messagePopup');
                self.setState({isLoadFile: false});
            } else if (res.data.error_code === 303) {
                self.props.addMessageText(res.data.text);
                // self.props.addMessageText('Файл не должен превышать 1 Мб');
                openPopupSimple('messagePopup');
                self.setState({isLoadFile: false});
            } else if (res.data.error_code === 304) {
                self.props.addMessageText(res.data.text);
                // self.props.addMessageText('Данный формат не может быть использован в качестве источника перевода');
                openPopupSimple('messagePopup');
                self.setState({isLoadFile: false});
            } else if (res.data.error_code === 999) {
                self.props.addMessageText(res.data.text);
                // self.props.addMessageText('Ошибка сервера');
                openPopupSimple('messagePopup');
                self.setState({isLoadFile: false});
            } else {
                self.setState({
                    addedFile: true,
                    countText: res.data.data.text !== '' ? res.data.data.text.trim().split(/\s+/).length : 0,
                    lang: {
                        logo: res.data.data.language_in.logo,
                        name: res.data.data.language_in.name,
                        id: res.data.data.language_in.id,
                    },
                    text: res.data.data.text,
                    count: res.data.data.count
                });
            }
        }).catch(error => {
            self.props.addMessageText(error.message);
            openPopupSimple('messagePopup');
            self.setState({isLoadFile: false});
        })
    }

    render () {
        const { countText, lang, addedFile, name, text, isLoadFile } = this.state;
        const countTextShow = countText === 0 || countText > 4 ? 'words' : countText === 1 ? 'word' : 'words';
        const isLoadFileContent = !isLoadFile ? 
                <div className="plus"></div> :
                <svg className="loader" width="44" height="44" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>
        return (
            <div>
                <section className="form_translate">
                    <p>First 500 characters come for free!</p>
                    
                    <div className={addedFile ? 'file_translate active' : 'file_translate'}>
                        <input type="file" id="add_file" className="add_file" onChange={this.handleFile}/>
                        <label for="add_file">
                            { isLoadFileContent }
                        </label>

                        <div className="field field_textarea">
                            <textarea placeholder="Введите текст" value={text} onChange={this.handleText}></textarea>
                            <div className="cancel" onClick={this.cancelFile}></div>
                            <div className="file_info">
                                <div className="name">{ name }</div>
                                <div className='lang'>
                                    <img src={ lang.logo } width="44" height="44" alt=""/>
                                    <div className="text">
                                        <div>{this.state.lang.name}</div>
                                        <div>{countText}&nbsp;{countTextShow}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="formats">Doc, docx, pdf, xls, csv, txt, djvu, rtf, ppt, pptx</div>
                    <div className="button_choose_wrapper">
                        <button className="btn btn_blue" onClick={this.sendText} disabled={!addedFile}>Translate</button>
                    </div>
                </section>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    csrf: state.csrf.token,
    text: state.sendTextToTranslate.text,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
})

const mapDispatchToProps = dispatch => ({
    addTextToTranslate: (text) => {
        dispatch(addTextToTranslate(text))
    },
    addCountToTranslate: (count) => {
        dispatch(addCountToTranslate(count))
    },
    addLanguageFromTranslate: (lang) => {
        dispatch(addLanguageFromTranslate(lang))
    },
    switchContent: (content) => {
        dispatch(switchContent(content))
    },
    addMessageText: (text) => dispatch(addMessageText(text)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})


export default connect(mapStateToProps, mapDispatchToProps)(TabsFiles);