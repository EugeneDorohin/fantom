import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { addTextToTranslate, addCountToTranslate, switchContent, addLanguageFromTranslate } from '../../../actions/sendTextToTranslateAction';
import { CHECK_LANGUAGES_API } from '../../../constants/post_url';
import { openPopupSimple, addMessageText } from '../../../actions/form';
import { getCsrf } from '../../../actions/csrfAction';

class TabsText extends Component {

    constructor (props) {
        super(props);
        let delay = null;
        this.state = {
            countText: 0,
            text: '',
            count: '',
            lang: {
                logo: '',
                name: '',
                id: ''
            },
            isNext: false
        }
    }

    handleText = (event) => {
        let val = event.currentTarget.value;
        this.setState({
            countText: val !== '' ? val.trim().split(/\s+/).length : 0,
            text: val,
            lang: {
                logo: '',
                name: 'Determine the language..'
            },
            isNext: false
        })
        
        clearTimeout(this.delay);

        this.delay = setTimeout (() => {
            if (val !== '') {
                this.defineLanguage();
            }
        }, 2000);

    }

    defineLanguage = () => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('text', this.state.text);
        
        axios({
            method: 'post',
            url: CHECK_LANGUAGES_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code !== 301) {
                self.setState({
                    lang: {
                        logo: res.data.data.language_in.logo,
                        name: res.data.data.language_in.name,
                        id: res.data.data.language_in.id,
                    },
                    count: res.data.data.count,
                    isNext: true
                });
            } else {
                self.setState({
                    lang: {
                        logo: '',
                        name: 'We can not translate'
                    },
                    isNext: false
                });
            }
        }).catch(error => {
            self.props.addMessageText(error.message);
            openPopupSimple('messagePopup');
        })
    }

    clearText = () => {
        this.setState({
            countText: 0,
            text: '',
            count: ''
        })
    }

    sendText = () => {
        window.scrollTo(0, 0);
        this.props.addTextToTranslate(this.state.text);
        this.props.addCountToTranslate(this.state.count);
        this.props.addLanguageFromTranslate(this.state.lang.id);
        this.props.switchContent('category');
    }
    
    render () {
        const { countText, text, lang } = this.state;
        const imageLanguages = lang.logo ? 
                <img src={ lang.logo } width="44" height="44" alt=""/> :
                <svg className="loader" width="44" height="44" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>
        const countTextShow = countText === 0 || countText > 4 ? 'words' : countText === 1 ? 'word' : 'words';
        return (
            <div>
                <section className="form_translate">
                    <p>First 500 characters come for free!</p>
                    <div className={'field field_textarea for_text_tab ' + (countText ? 'active' : '')} >
                        <textarea placeholder="Enter text" value={text} onChange={this.handleText}></textarea>
                        <div className={countText ? 'text_information active' : 'text_information'}>
                            <div className="lang">
                                { imageLanguages }
                                <div className="text">
                                    <div>{this.state.lang.name}</div>
                                    <div>{countText}&nbsp;{countTextShow}</div>
                                </div>
                            </div>
                            <div className="buttons">
                                <button className="btn btn_blue" onClick={this.sendText} disabled={!this.state.isNext}>Translate</button>
                                <button className="btn btn_blue" onClick={this.clearText}><img src={require('../../../images/close.svg')} width="20" height="20" alt=""/></button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    csrf: state.csrf.token,
    text: state.sendTextToTranslate.text,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
})

const mapDispatchToProps = dispatch => ({
    addTextToTranslate: (text) => {
        dispatch(addTextToTranslate(text))
    },
    addCountToTranslate: (count) => {
        dispatch(addCountToTranslate(count))
    },
    addLanguageFromTranslate: (lang) => {
        dispatch(addLanguageFromTranslate(lang))
    },
    switchContent: (content) => {
        dispatch(switchContent(content))
    },
    addMessageText: (text) => dispatch(addMessageText(text)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})


export default connect(mapStateToProps, mapDispatchToProps)(TabsText);