import React, { Component } from 'react';
import SendIco from '../../images/icons/send.svg';
import AcceptIco from '../../images/icons/accept.svg';
import { connect } from 'react-redux';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';
import axios from 'axios';
import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'
import { getCsrf } from '../../actions/csrfAction';
import { SEND_MESSAGE_API } from '../../constants/post_url';

class Contact extends Component {

    state = {
        isSended: false,
        email: '',
        message: '',
        valid: false
    }

    componentDidMount () {
        ReactGA.pageview(routes.CONTACT);
    }

    sendFeedback = (e) => {
        e.preventDefault();

        if (!this.state.valid) {
            [...document.querySelectorAll('.field')].map(item => {
                return (
                    item.classList.add('error')
                )
            });
            return false;
        }
        
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('email', this.state.email);
        listForm.append('text', this.state.message);
        axios({
            method: 'post',
            url: SEND_MESSAGE_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 101) {
                [...document.querySelectorAll('.email')].map(item => {
                    return (
                        item.classList.add('error')
                    )
                });
            } else {
                self.setState({
                    isSended: true
                });
                setTimeout(()=> {
                    self.setState({
                        isSended: false,
                        email: '',
                        message: ''
                    });
                }, 6000)
            }
        })
    }

    validateField = (event) => {
        let input = event.currentTarget,
            reg = /^.+@.+\..+$/i;
        if (input.name === 'email') {
            this.setState({
                email: input.value
            })
            if ( !reg.test(input.value) ) {
                input.parentNode.classList.add('error');
            } else {
                input.parentNode.classList.remove('error');
            }
        } else {
            this.setState({
                message: input.value
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if ( (prevState.message !== this.state.message) || (prevState.email !== this.state.email) ) {
            if ([this.state.email, this.state.message].includes('')) {
                this.setState({
                    valid: false
                })
            } else {
                this.setState({
                    valid: true
                })
            }
        }
    }

    render () {
        const { isSended, email, message, valid } = this.state;
        return (
            <div className="wrapper_page">
                <Header />
                <main>
                    <div className="page">
                        <section className="contact_page">
                            <h1 className="title_page">Contact</h1>
                            
                            <div className={isSended ? 'sended contact_form' : 'contact_form'}>
                                <form id="feedback_form" onSubmit={this.sendFeedback}>
                                    <div className="field email">
                                        <input type="email" name="email" placeholder="E-mail" value={email} onChange={this.validateField} />
                                    </div>
                                    <div className="field field_textarea">
                                        <textarea placeholder="Your message" name="message" value={message} onChange={this.validateField}></textarea>
                                    </div>
                                    <button id="send_form" className="send_btn" type="submit" disabled={!valid}>
                                        <img src={SendIco} width="25" height="25" alt="Send" />
                                    </button>
                                </form>
                                <div className="message_form">
                                    <img src={AcceptIco} width="56" height="56" alt="" />
                                    <div className="text">
                                        A reply will be sent to your email within 24 hours. If you have not received it, check the "Spam" tab. If you have not found anything, contact us again. Thank you.
                                    </div>
                                </div>
                            </div>
                            
                            
                        </section>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
})

const mapDispatchToProps = dispatch => ({
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})

export default connect(mapStateToProps, mapDispatchToProps)(Contact);