import React, { Component } from 'react';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'

class Rules extends Component {

    componentDidMount () {
        ReactGA.pageview(routes.RULES);
    }

    render () {
        return (
            <div className="wrapper_page">
                <Header />
                <main>
                    <div className="page">
                        <section className="rules">
                            <h1 className="title_page">Rules</h1>
                            <div className="description_rules">
                                All customers should familiarize themselves with the rules that come into force from 24.11.2018
                            </div>
                            <div className="row">
                                <div className="title_row">Definitions</div>
                                <div className="text">
                                    <p>
                                        <strong>Goldmean</strong> is an Intermediary between Translators and Customers, which creates all conditions for quick and effective work.
                                    </p>
                                    <p>
                                        <strong>Translator</strong> is a Person who knows two or more languages at a level higher than C.
                                    </p>
                                    <p>
                                        <strong>Customer</strong> is a Person or company whose goal is to translate a certain text.
                                    </p>
                                    <p>
                                        <strong>Comment to the translation</strong> is an Ability to specify key requirements in order to describe the translation in detail.
                                    </p>
                                </div>
                            </div>

                            <div className="row">
                                <div className="title_row">Introduction</div>
                                <div className="text">
                                    <ul>
                                        <li>
                                            <p>
                                                Goldmean should be used only for performing a text translation or as an aggregator of translation orders.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                It’s forbidden to exploit any bugs, errors, and imperfections; it’s necessary to inform the administration about their appearance.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="row">
                                <div className="title_row">Rules for customers</div>
                                <div className="text">
                                    <ul>
                                        <li>
                                            <p>
                                                The customer cannot challenge the price established for a certain text to a certain language.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                If the translation order did not contain a Comment to the translation, any subsequent specifications for it are not accepted.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                You can send a translation for revision 3 times and not more.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                The customer should not initiate disputes regarding a transaction with a payment system or a bank.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                The customer should use his (her) data for payment.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                When ordering a translation, specify your email address to which there is an access.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Don’t try to circumvent the opportunity to use the free test more times than it is written in paragraph 8.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Free test of 500 characters is given only once a week for an order of more than 2000 characters.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                The translation time displayed on the translation process page may differ from the real one.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                You should check the text after downloading the files as the file conversion may distort the text.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="row">
                                <div className="title_row">Rules for translators</div>
                                <div className="text">
                                    <ul>
                                        <li>
                                            <p>
                                                You must specify the languages and categories you work with at a level from C1.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Claims for translation payment rates are not accepted.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Claims for cancellation of part or full payment for translation are not accepted.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                We can block your account in case of suspicious activity, attempts to use the service to get more funds than necessary.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Funds may be blocked for an undefined period.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                In case you are not able to translate the text, you must switch to another.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Gaining a higher status will be only for outstanding performance in translation.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                You must use your real first and last name.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                You must use your real mailbox with access to it.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                If there is an error in the transfer of payment details, you must immediately contact us, but if payment is made by that time, any complaints will not be accepted.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Payment in case of submission of correct data must be made within 24 hours and in case of unforeseen circumstances within 72 hours.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Only you can use your account.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                It’s prohibited to create multiple accounts on the service.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="row">
                                <div className="title_row">Refund Policy</div>
                                <div className="text">
                                    <ul>
                                        <li>
                                            <p>
                                                Refund for the customer is possible if the translation was not completed within 48 hours.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Refund for the source text that was not originally associated with the requirements due to a comment to the translation is not accepted.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                Refund can be rejected in case you didn’t contact us regarding the failed translation after 48 hours.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                We do not accept claims and do not execute refunds if you did not check the text in the file and sent it for translation.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="row">
                                <div className="title_row">Privacy Policy</div>
                                <div className="text">
                                    <ul>
                                        <li>
                                            <p>
                                                Your text will be available only to translators, and in case of arising a problem to editors.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                The data of your mail address will not be passed on to third parties.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                We will send you notifications to your mailbox about the translation process and offers from us.
                                            </p>
                                        </li>
                                        <li>
                                            <p>
                                                We will use Cookies to provide an opportunity not to log in to your account permanently and to show the order process.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </section>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}

export default Rules;