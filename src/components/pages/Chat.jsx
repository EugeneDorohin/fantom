import React, { Component } from 'react';
import SendIco from '../../images/icons/send.svg';
import { connect } from 'react-redux';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';
import axios from 'axios';
import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'
import { getCsrf } from '../../actions/csrfAction';
import { GET_TICKET_API, GET_MESSAGES_API, SEND_MESSAGE_API, GET_CSRF_API } from '../../constants/post_url';

class Chat extends Component {

    constructor (props) {
        super(props);
        this.state = {
            isValid: true,
            list: null,
            message: ''
        }
        let timer = null,
            hash = null,
            email = null;
    }

    componentWillMount () {
        const getUrl = window.location.href;
        const urlObj = new URL(getUrl);
        this.hash = urlObj.searchParams.get('hash');
        this.email = urlObj.searchParams.get('email');

        if ([this.hash, this.email].includes(null)) {
            this.setState({
                isValid: false
            })
        } else {
            // this.getTicket(this.hash, this.email);
            this.getMessages(this.hash, this.email, 0, 0, 9999);
        }
    }

    componentDidMount () {
        ReactGA.pageview(routes.CHAT);
        setTimeout(()=>{
            let form = document.getElementById('form_send_message');
            window.scrollTo(0, form.offsetTop + form.offsetHeight - window.innerHeight);
        }, 300);
    }

    getTicket = (hash, email) => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('hash', hash);
        listForm.append('email', email);
        axios({
            method: 'post',
            url: GET_TICKET_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 401) {
                self.setState({
                    isValid: false
                });
            } else {
                console.log(res.data.data);
            }
        }) 
    }

    getMessages = (hash, email, start, start_from_id, length) => {
        const self = this;
        axios.post(GET_CSRF_API).then(function (res) {
            const listForm = new FormData();
            listForm.append('csrf', res.data.csrf);
            listForm.append('hash', hash);
            listForm.append('email', email);
            listForm.append('start', start);
            listForm.append('start_from_id', start_from_id);
            listForm.append('length', length);
            axios({
                method: 'post',
                url: GET_MESSAGES_API,
                data: listForm,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            }).then(function (res) {
                self.props.getCsrf(res.data.csrf);
                if (res.data.error_code === 401) {
                    self.setState({
                        isValid: false
                    });
                } else {
                    self.setState({
                        list: res.data.data.list
                    })
                    // let lastId = res.data.data.count + 1;
                    let lastId = 0;
                    self.timer = setTimeout(() => {
                        self.getMessages(hash, email, 0, lastId, 9999);
                    }, 15000);
                }
            }) 
        })
    }

    sendFeedback = (e) => {
        e.preventDefault();

        clearTimeout(this.timer);
        
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('email', this.email);
        listForm.append('text', this.state.message);
        axios({
            method: 'post',
            url: SEND_MESSAGE_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 101) {
                self.setState({
                    isValid: false
                });
            } else {
                self.setState({
                    message: ''
                })
                self.getMessages(self.hash, self.email, 0, 0, 9999);
            }
        })
    }

    changeTextarea = (e) => {
        this.setState({
            message: e.currentTarget.value
        })
    }

    render () {
        if (!this.state.isValid) {
            return (
                <div className="wrapper_page">
                    <Header />
                    <main>
                        <div className="page">
                            <div className="info_block">
                                <div className="ico"></div>
                                <p>
                                    Not valid hash or email
                                </p>
                            </div>
                        </div>
                        <HomeButton />
                    </main>
                </div>
            )
        }
        const messages = this.state.list ? this.state.list.map(message => (
            <div className={message.is_admin !== 0 ? "message" : "message my_message"}>
                <p>{message.text}</p>
            </div>
        )) : <svg className="loader" width="50" height="50" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536"><circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"></circle><circle fill="#29AEE3" cx="475" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"></circle><circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"></circle><circle fill="#29AEE3" cx="61" cy="268" r="59.9"></circle><circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"></circle></svg>;
        return (
            <div className="wrapper_page">
                <Header />
                <main>
                    <div className="page">
                        <section className="chat_page">
                            <h1 className="title_page">History</h1>
                            <div className="chat">
                                {messages}
                            </div>
                            <form id="form_send_message" onSubmit={this.sendFeedback}>
                                <textarea value={this.state.message} placeholder="Message" onChange={this.changeTextarea}></textarea>
                                <button id="send_message" className="send_btn" disabled={!this.state.message}>
                                    <img src={SendIco} width="25" height="25" alt="Send" />
                                </button>
                            </form>
                        </section>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
})

const mapDispatchToProps = dispatch => ({
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat);