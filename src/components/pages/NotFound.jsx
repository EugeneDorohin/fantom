import React, { Component } from 'react';

import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'

import * as routes from '../../constants/routes';
// import ReactGA from 'react-ga';

class About extends Component {
    componentDidMount () {
        // ReactGA.pageview(routes.ABOUT);
    }
    render () {
        return (
            <div id="page_404" className="wrapper_page">
                <main>
                    <div className="page">
                        <h1>404</h1>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}

export default About;