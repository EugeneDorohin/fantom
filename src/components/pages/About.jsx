import React, { Component } from 'react';

import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'

import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

class About extends Component {
    componentDidMount () {
        ReactGA.pageview(routes.ABOUT);
    }
    render () {
        return (
            <div className="wrapper_page">
                <Header />
                <main>
                    <div className="page">
                        <section className="about">
                            <h1 className="title_page">About us</h1>
                            <div className="row">
                                <div className="col image image_1"></div>
                                <div className="col">
                                    <div className="t">Dreamers</div>
                                    <div className="text">
                                        Searching for options of implementation of own desires and tasks, as well as desires and tasks of other people has become a goal to our team.
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="t">Perfectionists</div>
                                    <div className="text">
                                        If you do anything, you have to do it excellently!
                                    </div>
                                </div>
                                <div className="col image image_2"></div>
                            </div>
                            <div className="row">
                                <div className="col image image_3"></div>
                                <div className="col">
                                    <div className="t">Innovators</div>
                                    <div className="text">
                                        Only new things will calm us down. Only new things will improve us!
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="t">Optimists</div>
                                    <div className="text">
                                        It is only impossible until someone does it!
                                    </div>
                                </div>
                                <div className="col image image_4"></div>
                            </div>
                        </section>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}

export default About;