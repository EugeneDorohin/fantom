import React, { Component } from 'react';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

import Header from '../partials/Header';
import HomeButton from '../partials/Go_home'

class Help extends Component {

    state = {
        tab: true
    }

    componentDidMount () {
        ReactGA.pageview(routes.HELP);
    }

    changeTabs = tab => {
        this.setState({
            tab: tab
        })
    }

    render () {
        return (
            <div className="wrapper_page">
                <Header />
                <main>
                    <div className="page">
                        <section className="faq">
                            <h1 className="title_page">Help</h1>
                            <div className="tabs_block">
                                <div className={"tab" + (this.state.tab ? ' active': '')} onClick={() => {this.changeTabs(true)}}>Translation</div>
                                <div className={"tab" + (!this.state.tab ? ' active': '')} onClick={() => {this.changeTabs(false)}}>Translator</div>
                            </div>
                            {
                                this.state.tab ? (
                                    <div>
                                        <div className="row">
                                            <div className="question">
                                                When will I get my translation?
                                            </div>
                                            <div className="answer">
                                                The time of your translation depends on the amount of text and the availability of translators. You can be sure that it will be as short as possible.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How will I get my translation?
                                            </div>
                                            <div className="answer">
                                                You will get your translation to the mail address mentioned before payment and you will be able to download it immediately after the translation.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why my text cannot be translated?
                                            </div>
                                            <div className="answer">
                                                This is because we do not have translators now who could implement your order. In this case, please try a little later.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why is there no language I want to translate to?
                                            </div>
                                            <div className="answer">
                                                We probably do not have translators who are ready to translate to this language. Please try again within a few days.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                What if I entered the wrong email address?  
                                            </div>
                                            <div className="answer">
                                                No problem, wait until it is translated and just download it.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why do I need to write a comment to the translation? 
                                            </div>
                                            <div className="answer">
                                                So that translators can be sure of what you need, but it’s not necessary to write it.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                What if my translation did not meet my requirements?
                                            </div>
                                            <div className="answer">
                                                You can tell us about it with a red button, and then your translation will be checked for errors and inconsistencies by another translators.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                For how long can I report an improper translation?
                                            </div>
                                            <div className="answer">
                                                Within 48 hours
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Can I get a refund of my money?
                                            </div>
                                            <div className="answer">
                                                We try to do what you need, if we are not able to give it to you, we will refund your money. 
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How much text can I translate?
                                            </div>
                                            <div className="answer">
                                                We have no restrictions in this regard.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                What is the discount?
                                            </div>
                                            <div className="answer">
                                                Once a week you can get 500 characters free when ordering at least 2000.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Can I select multiple languages to translate?
                                            </div>
                                            <div className="answer">
                                                No, you cannot now. You will have to make several orders for different languages. However, we work so that it becomes possible.
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div>
                                        <div className="row">
                                            <div className="question">
                                                How can I withdraw funds?
                                            </div>
                                            <div className="answer">
                                                We have three ways: Webmoney, Paypal, and Bitcoin.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                When will my funds be available for receipt?
                                            </div>
                                            <div className="answer">
                                                Immediately, however, we need 24 hours for their withdrawal.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why are the funds for the translation blocked?
                                            </div>
                                            <div className="answer">
                                                We got a complaint about the translation. We need time to know the circumstances, then you can get the funds if you did everything correctly on your part.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How to get a raised level?
                                            </div>
                                            <div className="answer">
                                                To perform many translations and to do them well.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why can't I find the text to translate?
                                            </div>
                                            <div className="answer">
                                                Maybe there is not yet, or it is given to translators who have higher experience and quality of translation than you, please wait for it to appear and do it as accurately as possible.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How long does it take to receive text for translation?
                                            </div>
                                            <div className="answer">
                                                It depends on the number of orders and your experience on the service. 
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why should I accept notifications?
                                            </div>
                                            <div className="answer">
                                                You can find out when there is an order for you.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Will I receive a translation if I close the browser?
                                            </div>
                                            <div className="answer">
                                                If you agree to the notification, we will send you a new order.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why was my account blocked?
                                            </div>
                                            <div className="answer">
                                                This means that your translation received a low rating or many complaints.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Can I have multiple accounts?
                                            </div>
                                            <div className="answer">
                                                It will be unprofitable for you. In order to work successfully you need to earn a rating, which consists of experience and quality of translation.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How many languages and categories can I select?
                                            </div>
                                            <div className="answer">
                                                We have no restrictions on this.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How much money will I get for the translation?
                                            </div>
                                            <div className="answer">
                                                It depends on many factors, so we are not able to tell you the amount.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                Why was I automatically switched from translation?
                                            </div>
                                            <div className="answer">
                                                You must begin to translate immediately, continuously translating and finish in a given time, if something is not observed, we will give the text to another translator.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                How can I change the languages and categories I want to translate?
                                            </div>
                                            <div className="answer">
                                                Now this feature is not available, but soon we will add it.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                What are the Verifier’s responsibilities?
                                            </div>
                                            <div className="answer">
                                                The task is to evaluate the already completed translation using five stars, the better the translation, the more stars it should receive.
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="question">
                                                What are the Editor’s responsibilities?
                                            </div>
                                            <div className="answer">
                                                Edit the already completed translation according to customer’s requirements, which are given in the comments to the translation. This is the most responsible position!
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                            
                            
                        </section>
                    </div>
                    <HomeButton />
                </main>
            </div>
        );
    }
}

export default Help;