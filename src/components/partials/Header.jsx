import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import * as routes from '../../constants/routes';
import Nav from './Nav';
import logo from '../../images/logo.svg';


class Header extends Component {

    
    render () {
        return (
            <header className="simple_header">
                <div className="page">
                    <NavLink exact to={routes.HOME} className="logo">
                        GM
                        {/* <img src={logo} width="47" height="47" alt="" /> */}
                    </NavLink>
                    <Nav />
                </div>
            </header>
        );
    }
}

export default Header;