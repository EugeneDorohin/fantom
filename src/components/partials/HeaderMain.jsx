import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';

import * as routes from '../../constants/routes';
import logo from '../../images/logo.svg';
import login from '../../images/icons/login.svg';
import logout from '../../images/icons/logout.svg';

import { openPopup, activeFromHeader } from '../../actions/form';
import { EXIT_API } from '../../constants/post_url';
import { LoginAction } from '../../actions/userAction';
import { switchContent } from '../../actions/sendTextToTranslateAction';
import { getCsrf } from '../../actions/csrfAction';
import { takeAnOrder } from '../../actions/get_orderAction';


class HeaderMain extends Component {

    logOut = (e) => {
        e.preventDefault();
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        axios({
            method: 'post',
            url: EXIT_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            self.props.logOut();
            self.props.history.push(routes.HOME);
            self.props.takeAnOrder(true);
            window.scrollTo(0, 0);
        })    
    }

    loginForm = (target) => {
        openPopup('userForm', target, 'from_header')
        this.props.activeFromHeader(true);
    }

    defaultState = () => {
        this.props.switchContent('text');
    }
    

    render () {
        const { isLoggedIn } = this.props.user;
        const { userData } = this.props.user;
        const userActivity = isLoggedIn ? 
            <a href="#" className="logout" onClick={this.logOut}>
                <img src={logout} width="30" height="30" alt="" />
            </a> :
            <a href="#" className="login" onClick={this.loginForm}>
                <img src={login} width="30" height="30" alt="" />
            </a>
        const centerContent = !isLoggedIn ?
                        <div className="center">
                            <NavLink exact to={routes.HOME} activeClassName="current">Translate</NavLink>
                            <NavLink to={routes.TRANSLATOR} activeClassName="current">Translator</NavLink>
                        </div> :
                        window.location.pathname === routes.BALANCE ?
                            <NavLink to={routes.HOME} className="user_menu" onClick={this.defaultState}>To translation</NavLink>
                            :
                            <NavLink to={routes.BALANCE} className="user_menu">${ userData.balance }</NavLink>
        return (
            <header>
                <div className="page">
                    <NavLink exact to={this.props.isOrder && isLoggedIn ? routes.WORK : routes.HOME} onClick={this.defaultState} className="logo">
                        GM
                        {/* <img src={logo} width="47" height="47" alt="" /> */}
                    </NavLink>
                    { centerContent }
                    <div className="user">
                        { userActivity }
                    </div>
                </div>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
    csrf: state.csrf.token,
    isOrder: state.get_order.isOrder
})
const mapDispatchToProps = dispatch => ({
    logOut: () => dispatch(LoginAction(false)),
    activeFromHeader: (data) => dispatch(activeFromHeader(data)),
    switchContent: (content) => {
        dispatch(switchContent(content))
    },
    takeAnOrder: (isOrder) => dispatch(takeAnOrder(isOrder)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMain);