import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import * as routes from '../../constants/routes';
import Nav from './Nav';
import logo from '../../images/logo.svg';
import { switchContent } from '../../actions/sendTextToTranslateAction';

class Footer extends Component {

    defaultState = () => {
        this.props.switchContent('text');
        window.scrollTo(0, 0);
    }


    render () {
        return (
            <footer>
                <div className="page">
                    <NavLink exact to={routes.HOME} className="logo" onClick={this.defaultState}>
                        GM
                        {/* <img src={logo} width="37" height="37" alt="" /> */}
                    </NavLink>
                    <Nav />
                </div>
            </footer>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    contentSwitch: state.sendTextToTranslate.contentSwitch,
    isOrder: state.get_order.isOrder
})
const mapDispatchToProps = dispatch => ({
    switchContent: (content) => {
        dispatch(switchContent(content))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Footer);