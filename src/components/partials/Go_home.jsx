import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import * as routes from '../../constants/routes';

class HomeButton extends Component {
    render () {
        return (
            <div className="page">
                <div className="wrapper_go_home">
                    <NavLink to={routes.HOME} onClick={() => window.scrollTo(0, 0)} className="go_home btn">Go Home</NavLink>
                </div>
            </div>
        );
    }
}

export default HomeButton;