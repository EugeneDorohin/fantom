import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import * as routes from '../../constants/routes';

class Nav extends Component {
    render () {
        return (
            <nav>
                <NavLink activeClassName="active" to={routes.ABOUT} onClick={() => window.scrollTo(0, 0)}>About us</NavLink>
                <NavLink activeClassName="active" to={routes.HELP} onClick={() => window.scrollTo(0, 0)}>Help</NavLink>
                <NavLink activeClassName="active" to={routes.CONTACT} onClick={() => window.scrollTo(0, 0)}>Contact us</NavLink>
                <NavLink activeClassName="active" to={routes.RULES} onClick={() => window.scrollTo(0, 0)}>Rules</NavLink>
            </nav>
        );
    }
}

export default Nav;