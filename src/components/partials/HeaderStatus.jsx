import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import * as routes from '../../constants/routes';
import logo from '../../images/logo.svg';
import { switchContent } from '../../actions/sendTextToTranslateAction';


class HeaderStatus extends Component {
    
    render () {
        const textStatus = this.props.left_time ? this.props.left_time : <span>{this.props.text}</span>
        return (
            <header>
                <div className="page">
                    <NavLink exact to={routes.HOME} onClick={() => this.props.switchContent('text')} className="logo">
                        GM
                        {/* <img src={logo} width="47" height="47" alt="" /> */}
                    </NavLink>
                    <div className="status_work">{textStatus}</div>
                </div>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    contentSwitch: state.sendTextToTranslate.contentSwitch
})
const mapDispatchToProps = dispatch => ({
    switchContent: (content) => {
        dispatch(switchContent(content))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(HeaderStatus);