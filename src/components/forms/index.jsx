import React, { Component } from 'react';

import { NavLink } from 'react-router-dom';
import Login from './Login';
import Registration from './Registration';
import * as routes from '../../constants/routes';
import { connect } from 'react-redux';
import { activeFromHeader } from '../../actions/form';
import { switchRegContent } from '../../actions/userAction';


class Form extends Component {

    state = {
        login: false,
        text: 'I don’t have a profile'
    }

    changeTab = event => {
        event.preventDefault();
        this.setState({
            login: !this.state.login,
            text: !this.state.login ? 'I don’t have a profile' : 'I have a profile'
        })
    }

    closePopup = (event) => {
        if (event) event.preventDefault();
        document.body.classList.remove('no_scroll');
        [...document.querySelectorAll('.popup_wrapper')].map(item => {
            return (
                item.classList.remove('open_popup')
            )
        });
        setTimeout(()=> {
            this.setState({
                login: false,
                text: 'I don’t have a profile'
            })
            this.props.activeFromHeader(false);
            [...document.querySelectorAll('.popup_wrapper')].map(item => {
                return (
                    item.classList.remove('from_header')
                )
            });
        }, 501);
    }

    pushReg = () => {
        this.closePopup();
        this.props.switchRegContent(true);
    }

    render () {
        const { login, text } = this.state;
        let contentPopup = login === true || this.props.form.fromHeader === true ? <Login /> : <Registration /> ;
        
        return (
            <div className="popup_wrapper" data-popup="userForm">
                <div className="bg" onClick={this.closePopup}></div>
                <div className="popup">
                    <div className="close">
                        <button onClick={this.closePopup}>
                            <svg width="19" height="19" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                            </svg>
                        </button>
                    </div>
                    <div className="content_popup">
                        { contentPopup }
                        {/* <div className="link_tab">
                            <a href="#" onClick={this.changeTab}>{text}</a>
                        </div> */}
                        <div className="link_tab link_registration">
                            <NavLink to={routes.TRANSLATOR} onClick={this.pushReg}>{text}</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
})

const mapDispatchToProps = dispatch => ({
    activeFromHeader: (data) => dispatch(activeFromHeader(data)),
    switchRegContent: (content) => {
      dispatch(switchRegContent(content))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Form);