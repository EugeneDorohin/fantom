import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closePopup } from '../../actions/form';
import { takeAnOrder } from '../../actions/get_orderAction';

class Message extends Component {

    close = () => {
        this.props.takeAnOrder(false);
        closePopup();
    }

    render () {
        
        return (
            <div className="popup_wrapper" data-popup="messagePopup">
                <div className="bg" onClick={this.close}></div>
                <div className="popup">
                    <div className="content_popup">
                        <div className="message_content">
                            <div className="ico"></div>
                            <div className="text">
                                {this.props.form.messageText}
                            </div>
                            <button className="btn btn_blue" onClick={this.close}>Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    isOrder: state.get_order.isOrder
})

const mapDispatchToProps = dispatch => ({
    takeAnOrder: (isOrder) => dispatch(takeAnOrder(isOrder))
})

export default connect(mapStateToProps, mapDispatchToProps)(Message);