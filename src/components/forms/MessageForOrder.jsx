import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closePopup } from '../../actions/form';

class MessageForOrder extends Component {
    
    
    cancelOrder = () => {
        this.props.cancelOrder();
    }

    render () {       
        return (
            <div className="popup_wrapper" data-popup="messageOrderPopup">
                <div className="bg" onClick={closePopup}></div>
                <div className="popup">
                    <div className="content_popup">
                        <div className="message_content for_order">
                            <div className="ico"></div>
                            <div className="text">
                                {this.props.form.messageText}
                            </div>
                            <div className="wrapper_buttons">
                                <button className="btn" onClick={this.cancelOrder}>Ok</button>
                                <button className="btn btn_blue" onClick={closePopup}><i className="close"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
})

export default connect(mapStateToProps)(MessageForOrder);