import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import * as routes from '../../constants/routes';

import { LoginAction } from '../../actions/userAction';
import { getCsrf } from '../../actions/csrfAction';
import { AUT_API, GET_CSRF_API } from '../../constants/post_url';
import { addMessageText } from '../../actions/form';

class Login extends Component {

    state = {
        login: '',
        password: ''
    }

    validateField = (event) => {
        let input = event.currentTarget,
            reg = /^.+@.+\..+$/i;
        if (input.name === 'emailLogin') {
            this.setState({
                login: input.value
            })
            if ( !reg.test(input.value) ) {
                input.parentNode.classList.add('error');
            } else {
                input.parentNode.classList.remove('error');
            }
        } else {
            this.setState({
                password: input.value
            })
        }
    }

    handleFocus = (e) => {
        e.currentTarget.parentNode.classList.remove('error');
    }

    submitLogin = (e) => {
        e.preventDefault();
        if (this.state.login === '' || this.state.password === '') {
            [...document.querySelectorAll('.inputLogin')].map(item => {
                return (
                    item.classList.add('error')
                )
            });
            return false;
        }
        const listForm = new FormData();
        
        listForm.append('csrf', this.props.csrf);
        listForm.set('login', this.state.login);
        listForm.set('password', this.state.password);
        
        this.props.loginSubmitToServer(listForm, this.props);
    }

    cancelError = () => {
        [...document.querySelectorAll('.login_error')].map(item => {
            item.classList.remove('show');
        }); 
    }
    
    render () {
        return (
            <form onSubmit={this.submitLogin}>
                <div className="form_sender">
                    <div className="title">Log in</div>
                    <div className="field inputLogin">
                        <input type="email" name="emailLogin" placeholder="E-mail" onChange={this.validateField} onFocus={this.handleFocus} />
                    </div>
                    <div className="field inputLogin">
                        <input type="password" name="passwordLogin" placeholder="Password" onChange={this.validateField} onFocus={this.handleFocus} />
                    </div>
                    <button className="btn btn_blue" type="submit">Log in</button>
                    <div className="error_popup_message login_error">
                        <div className="ico"></div>
                        <div className="text">{this.props.form.messageText}</div>
                        <button className="btn btn_blue" type="button" onClick={this.cancelError}>Try again</button>
                    </div>
                </div>
            </form>
        );
    }
}

export const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
})
  
export const mapDispatchToProps = dispatch => ({
    loginSubmitToServer: (loginItems, props) => axios({
        method: 'post',
        url: AUT_API,
        data: loginItems,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
        if (res.data.error_code === 103) {
            props.addMessageText(res.data.text);
            // props.addMessageText('Почтовый адрес или пароль указаны не верно');
            [...document.querySelectorAll('.login_error')].map(item => {
                item.classList.add('show');
            });
        } else {
            window.location.href = routes.WORK;
        }
        dispatch(getCsrf(res.data.csrf))
    }),
    // getCsrf: axios.post(GET_CSRF_API).then(function (res) {
    //     dispatch(getCsrf(res.data.csrf))
    // }),
    addMessageText: (text) => dispatch(addMessageText(text)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Login);