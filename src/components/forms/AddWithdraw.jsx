import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { closePopup, addMessageText, openPopupSimple } from '../../actions/form';
import { ADD_WITHDRAW_API, GET_CSRF_API } from '../../constants/post_url';
import { getCsrf } from '../../actions/csrfAction';

class AddWithdraw extends Component {
    
    state = {
        value: '',
        isError: false,
        error: '',
        valid: false
    }

    validateField = (event, name) => {

        
        let input = event.currentTarget,
        wmz = /^z\d{12}$/i,
        paypal = /^.+@.+\..+$/i,
        btc = /^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$/i;
        
        this.setState({
            value: input.value
        })
        
        if (name == 'Webmoney WMZ') {
            if ( !wmz.test(input.value) ) {
                input.parentNode.classList.add('error');
                this.setState({
                    valid: false
                })
            } else {
                input.parentNode.classList.remove('error');
                this.setState({
                    valid: true
                })
            }
        }
        if (name == 'PayPal') {
            if ( !paypal.test(input.value) ) {
                input.parentNode.classList.add('error');
                this.setState({
                    valid: false
                })
            } else {
                input.parentNode.classList.remove('error');
                this.setState({
                    valid: true
                })
            }
        }
        if (name == 'BitCoin') {
            if ( !btc.test(input.value) ) {
                input.parentNode.classList.add('error');
                this.setState({
                    valid: false
                })
            } else {
                input.parentNode.classList.remove('error');
                this.setState({
                    valid: true
                })
            }
        }
    }

    handleFocus = (e) => {
        this.setState({
            isError: false,
            error: ''
        })
        e.currentTarget.parentNode.classList.remove('error');
    }
    
    send = (id) => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('system_id', id);
        listForm.append('summ', this.props.user.userData.balance);
        listForm.append('address', this.state.value);
        axios({
            method: 'post',
            url: ADD_WITHDRAW_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                // openPopupSimple('messagePopup');
                self.setState({
                    isError: true,
                    error: res.data.text
                })
            } else if (res.data.error_code === 403) {
                // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                // openPopupSimple('messagePopup');
                self.setState({
                    isError: true,
                    error: res.data.text
                })
            } else if (res.data.error_code === 501) {
                self.setState({
                    isError: true,
                    error: res.data.text
                })
            } else {
                if (res.data.data.id) {
                    self.props.paymentRequestReceived();
                }
            }
        })
    }

    closePopup = (event) => {
        if (event) event.preventDefault();
        document.body.classList.remove('no_scroll');
        [...document.querySelectorAll('.popup_wrapper')].map(item => {
            return (
                item.classList.remove('open_popup')
            )
        });
        setTimeout(()=> {
            this.setState({
                value: '',
                isError: false,
                valid: false
            })
        }, 501);
    }

    render () { 
        const {name, id} = this.props;      
        return (
            <div className="popup_wrapper" data-popup="addWithdraw">
                <div className="bg" onClick={this.closePopup}></div>
                <div className="popup">
                    <div className="close">
                        <button onClick={this.closePopup}>
                            <svg width="19" height="19" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                            </svg>
                        </button>
                    </div>
                    <div className="content_popup">
                        <div className="title">Payment Details</div>
                        <div>
                            <div className="field">
                                <input type="text" placeholder={name} onChange={(e) => this.validateField(e, name)} onFocus={this.handleFocus} value={this.state.value} />
                            </div>
                        </div>
                        <div className="wrapper_buttons error_wrapper">
                            <button className="btn btn_blue" onClick={() => this.send(id)} disabled={!this.state.valid}>Pay</button>
                            <p className={this.state.isError ? "error_text active" : "error_text"} >{this.state.error}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
});

const mapDispatchToProps = dispatch => ({
    addMessageText: (text) => dispatch(addMessageText(text)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddWithdraw);