import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import axios from 'axios';

import { LoginAction } from '../../actions/userAction';
import { getCsrf } from '../../actions/csrfAction';
import { REG_API, GET_CSRF_API } from '../../constants/post_url';

import * as routes from '../../constants/routes';
import { addMessageText } from '../../actions/form';



class Registration extends Component {

    state = {
        email: '',
        password: '',
        name: ''
    }

    validateField = (event) => {
        let input = event.currentTarget,
            reg = /^.+@.+\..+$/i;
        if (input.name === 'reg_e') {
            this.setState({
                email: input.value
            })
            if ( !reg.test(input.value) ) {
                input.parentNode.classList.add('error');
            } else {
                input.parentNode.classList.remove('error');
            }
        } else if (input.name === 'nameRegistration') {
            this.setState({
                name: input.value
            })
        } else {
            this.setState({
                password: input.value
            })
        }
    }

    handleFocus = (e) => {
        e.currentTarget.parentNode.classList.remove('error');
    }

    submitLogin = (e) => {
        e.preventDefault();
        if (this.state.login === '' || this.state.password === '' || this.state.name.length < 2) {
            [...document.querySelectorAll('.inputRegistration input')].map(item => {
                return (
                    item.value === '' ? item.classList.add('error') : ''
                )
            });
            return false;
        }
        const listForm = new FormData();
        
        listForm.append('csrf', this.props.csrf);
        for (let i = 0; i < this.props.languages.choose_languages.length; i++) {
            listForm.append('language_in[]', this.props.languages.choose_languages[i]);
            listForm.append('language_out[]', this.props.languages.choose_languages[i]);
        }
        for (let i = 0; i < this.props.categories.choose_categories.length; i++) {
            listForm.append('category[]', this.props.categories.choose_categories[i]);
        }
        listForm.append('email', this.state.email);
        listForm.append('password', this.state.password);
        listForm.append('name', this.state.name);
        
        this.props.registrationSubmitToServer(listForm, this.props);
    }

    cancelError = () => {
        [...document.querySelectorAll('.registration_error')].map(item => {
            item.classList.remove('show');
        }); 
    }
    
    render () {
        return (
            <form onSubmit={this.submitLogin}>
                <div className="form_sender">
                    <div className="title">Registration</div>
                    <div className="field inputRegistration">
                        <input type="text" placeholder="Name and Last Name" autocomplete="off" name="nameRegistration" onChange={this.validateField} onFocus={this.handleFocus} />
                    </div>
                    <div className="field inputRegistration emailRegistration">
                        <input type="email" placeholder="E-mail" autocomplete="off" name="reg_e" className="hidden_input" />
                        <input type="email" placeholder="E-mail" autocomplete="off" name="reg_e" onChange={this.validateField} onFocus={this.handleFocus} />
                    </div>
                    <div className="field inputRegistration passRegistration">
                        <input type="password" placeholder="Password" autocomplete="off" name="reg_p" className="hidden_input" />
                        <input type="password" placeholder="Password" autocomplete="off" name="reg_p" onChange={this.validateField} onFocus={this.handleFocus} />
                    </div>
                    <button className="btn btn_blue registration_btn">Registration</button>
                    <div className="error_popup_message registration_error">
                        <div className="ico"></div>
                        <div className="text">{this.props.form.messageText}</div>
                        <button className="btn btn_blue" type="button" onClick={this.cancelError}>Try again</button>
                    </div>
                </div>
                
                <p>
                    By registering, you accept the <Link to={routes.RULES} target="_blank">Terms and Conditions</Link>
                </p>
            </form>
        );
    }
}

export const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token
})
  
export const mapDispatchToProps = dispatch => ({
    registrationSubmitToServer: (loginItems, props) => axios({
        method: 'post',
        url: REG_API,
        data: loginItems,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
    }).then(function (res) {
        if (res.data.error_code === 101) {
            props.addMessageText(res.data.text);
            // props.addMessageText('Не корректный email');
            [...document.querySelectorAll('.registration_error')].map(item => {
                item.classList.add('show');
            });
        } else if (res.data.error_code === 102) {
            props.addMessageText(res.data.text);
            // props.addMessageText('Пароль меньше 6 символов');
            [...document.querySelectorAll('.registration_error')].map(item => {
                item.classList.add('show');
            });
        } else if (res.data.error_code === 104) {
            props.addMessageText(res.data.text);
            // props.addMessageText('Данный email уже зарегистрирован в системе');
            [...document.querySelectorAll('.registration_error')].map(item => {
                item.classList.add('show');
            });
        } else if (res.data.error_code === 302) {
            props.addMessageText(res.data.text);
            // props.addMessageText('Не удалось зарегистрировать пользователя на поиск работы, проверьте вводные данные');
            [...document.querySelectorAll('.registration_error')].map(item => {
                item.classList.add('show');
            });
        } else {
            window.location.href = routes.WORK;
        }
        dispatch(getCsrf(res.data.csrf));
    }),
    // getCsrf: axios.post(GET_CSRF_API).then(function (res) {
    //     dispatch(getCsrf(res.data.csrf))
    // }),
    addMessageText: (text) => dispatch(addMessageText(text))
})


export default connect(mapStateToProps, mapDispatchToProps)(Registration);