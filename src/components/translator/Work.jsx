import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import ReactTooltip from 'react-tooltip';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';
import Sound from 'react-sound';
import soundfile from '../../sounds/work.mp3';

import HeaderMain from '../partials/HeaderMain';
import { GET_ORDER_API, GET_ORDER_ID_API, SET_ORDER_ID_API, GET_CSRF_API, GET_ORDER_CANCEL_API, GET_NOTIFICATIONS_API, SET_NOTIFY_ID_API } from '../../constants/post_url';
import { takeAnOrder } from '../../actions/get_orderAction';
import MessageForOrder from '../forms/MessageForOrder';
import { addMessageText, openPopupSimple } from '../../actions/form';
import { getCsrf } from '../../actions/csrfAction';

import TextareaAutosize from 'react-autosize-textarea';
import CompleteForm from './partials/form/CompleteForm';

class Work extends Component {

    constructor(props) {
        super(props);
        let delay = null;
        let timerForGetOrderId = null;
        let interval = null;
        let intervalTime = null;
        this.state = {
            workList: null,
            text_out: '',
            wait_time: null,
            wait_input: null,
            cancelType: '',
            paused: localStorage.getItem('paused') || 'false',
            notifications: null,
            isClickSendOrder: 0
        }
    }

    componentDidMount () {
        if (this.props.user.isLoggedIn === true) {
            
            ReactGA.pageview(routes.WORK);

            if (this.interval) clearInterval(this.interval);
            if (this.intervalTime) clearInterval(this.intervalTime);
            
            this.findWork();
            this.interval = setInterval (()=>{
                if (this.props.get_order.isOrder === true || this.state.paused === 'true') {
                    return false;
                } else {
                    this.findWork();
                }
            }, 5000);
            
            this.timerFormWork();

            this.getNotifications();
        }
    }

    timerFormWork = () => {
        if (this.intervalTime) clearInterval(this.intervalTime);
        this.intervalTime = setInterval (()=>{
            if (this.props.get_order.isOrder === true && this.state.paused === 'false') {
                this.setState({
                    wait_time: this.state.wait_time > 0 ? this.state.wait_time - 1 : 0,
                    wait_input: this.state.wait_input > 0 ? this.state.wait_input - 1 : 0
                })
                if (this.state.wait_time == 0 || this.state.wait_input == 0 ) {
                    this.props.takeAnOrder(false);
                }
            }
        }, 1000);
    }

    getNotifications = () => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('only_new', 1);
        listForm.append('start', 0);
        listForm.append('length', 1000);
        axios({
            method: 'post',
            url: GET_NOTIFICATIONS_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            self.setState({
                notifications: res.data.data
            })
            // if (res.data.data.count != '0') {
            // }
        });
    }
    
    closeNotify = (id, id_item) => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('id', id);
        axios({
            method: 'post',
            url: SET_NOTIFY_ID_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            // document.getElementById(id_item).remove();
            self.getNotifications();
        });
    }

    findWork = () => {
        if (this.state.paused === 'true') {
            return false;
        }
        if (window.location.pathname !== routes.WORK) {
            return false;
        }
        this.setState({
            isClickSendOrder: 0
        })
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        axios({
            method: 'post',
            url: GET_ORDER_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else {
                try {
                    if (res.data.data.id) {
                        const listFormID = new FormData();
                        listFormID.append('csrf', res.data.csrf);
                        listFormID.append('id', res.data.data.id);
                        if (window.location.pathname === routes.WORK) {
                            self.getOrderId(listFormID);
                        }
                    }
                } catch (err) {

                }
            }
            
        })
    }

    singlePush = () => {
        this.myRef = React.createRef();
        return (
            <audio ref={this.myRef} src={soundfile} autoPlay/>
        )
    }
    
    getOrderId = (listForm) => {
        const self = this;
        axios({
            method: 'post',
            url: GET_ORDER_ID_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 402) {
                // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
                // self.props.addMessageText(res.data.text);
                // openPopupSimple('messagePopup');
                self.props.takeAnOrder(false);
                document.body.classList.remove('no_scroll');
            } else if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else {
                self.setState({
                    workList: res.data.data,
                    text_out: res.data.data.text_out,
                    wait_time: res.data.data.wait_time,
                    wait_input: res.data.data.wait_input,
                })
                self.props.takeAnOrder(true);
                if (self.timerForGetOrderId) clearTimeout(self.timerForGetOrderId);
                // self.getOrderIdInfo(listForm);
            }
        })
    }

    // getOrderIdInfo = (listForm) => {
    //     if (this.state.isClickSendOrder === 1) {
    //         return false;
    //     }
    //     const self = this;
    //     axios({
    //         method: 'post',
    //         url: GET_ORDER_ID_API,
    //         data: listForm,
    //         config: { headers: {'Content-Type': 'multipart/form-data' }}
    //     }).then(function (res) {
    //         self.props.getCsrf(res.data.csrf);
    //         if (res.data.error_code === 402) {
    //             // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
    //             // self.props.addMessageText(res.data.text);
    //             // openPopupSimple('messagePopup');
    //             self.props.takeAnOrder(false);
    //             document.body.classList.remove('no_scroll');
    //         } else if (res.data.error_code === 201) {
    //             // self.props.addMessageText('Пользователь не авторизован');
    //             self.props.addMessageText(res.data.text);
    //             openPopupSimple('messagePopup');
    //         } else {
    //             self.timerForGetOrderId = setTimeout(()=>{
    //                 self.getOrderIdInfo(listForm);
    //             }, 15000);
    //         }
    //     })
    // }

    handleText = (event) => {
        let val = event.currentTarget.value;
        this.setState({
            text_out: val
        })
        
        clearTimeout(this.delay);

        this.delay = setTimeout (() => {
            this.sendSetTextOut(val);
        }, 1000);

    }

    sendSetTextOut = (val) => {
        const self = this;
        axios.post(GET_CSRF_API).then(function (res) {
            const listForm = new FormData();
            listForm.append('csrf', res.data.csrf);
            listForm.append('id', self.state.workList.id);
            listForm.append('action', 'set_text_out');
            listForm.append('text_out', val);
            axios({
                method: 'post',
                url: SET_ORDER_ID_API,
                data: listForm,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            }).then(function (res) {
                self.props.getCsrf(res.data.csrf);
                if (res.data.error_code === 402) {
                    // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else if (res.data.error_code === 201) {
                    // self.props.addMessageText('Пользователь не авторизован');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else if (res.data.error_code === 403) {
                    // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else {
                    self.setState({
                        wait_time: res.data.data.wait_time,
                        wait_input: res.data.data.wait_input
                    })
                }
            })
        })
    }

    pauseOrder = () => {
        this.setState({
            cancelType: 'pause'
        })
        this.props.addMessageText('If you put the work on hold, your translation will be lost');
        openPopupSimple('messageOrderPopup');
    }

    nextOrder = () => {
        this.setState({
            cancelType: 'next'
        })
        this.props.addMessageText('If you skip the work, your translation will be lost');
        openPopupSimple('messageOrderPopup');
    }

    pausedFunction = () => {
        const self = this;
        axios.post(GET_CSRF_API).then(function (res) {
            const listForm = new FormData();
            listForm.append('csrf', res.data.csrf);
            axios({
                method: 'post',
                url: GET_ORDER_CANCEL_API,
                data: listForm,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            }).then(function (res) {
                self.props.getCsrf(res.data.csrf);
                if (res.data.error_code === 201) {
                    // self.props.addMessageText('Пользователь не авторизован');
                    self.props.addMessageText(res.data.text);
                    openPopupSimple('messagePopup');
                } else {
                    localStorage.setItem('paused', 'true')
                    self.setState({
                        paused: 'true'
                    })
                    self.props.takeAnOrder(true);
                }
            })
        })
    }

    cancelOrder = () => {
        if (this.state.cancelType === 'pause') {
            this.pausedFunction()
        }
        if (this.state.cancelType === 'next') {
            this.skipOrder();
            this.props.takeAnOrder(false);
        }
        document.body.classList.remove('no_scroll');
    }
    
    completeOrderFrom = () => {
        if (this.state.isClickSendOrder === 1) {
            return false;
        }
        this.setState({
            isClickSendOrder: 1
        })
        openPopupSimple('completeForm');

        // if (this.intervalTime) clearInterval(this.intervalTime);
        clearTimeout(this.delay);
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('id', this.state.workList.id);
        listForm.append('action', 'set_translate_end');
        listForm.append('text_out', this.state.text_out);
        axios({
            method: 'post',
            url: SET_ORDER_ID_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 402) {
                // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 403) {
                // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else {
                
            }
        })
    }

    completeOrder = () => {
        this.props.takeAnOrder(false);
        // this.timerFormWork();
    }

    setStars = (star) => {
        openPopupSimple('completeForm');

        // if (this.intervalTime) clearInterval(this.intervalTime);
        clearTimeout(this.delay);
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('id', this.state.workList.id);
        listForm.append('action', 'set_stars');
        listForm.append('stars', star);
        axios({
            method: 'post',
            url: SET_ORDER_ID_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 402) {
                // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 403) {
                // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else {
                // self.props.takeAnOrder(false);
            }
        })
    }

    skipOrder = () => {
        const self = this;
        const listForm = new FormData();
        listForm.append('csrf', this.props.csrf);
        listForm.append('id', this.state.workList.id);
        listForm.append('action', 'set_cancel');
        axios({
            method: 'post',
            url: SET_ORDER_ID_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            self.props.getCsrf(res.data.csrf);
            if (res.data.error_code === 402) {
                // self.props.addMessageText('Нет доступа к запрашиваемому заказу');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 201) {
                // self.props.addMessageText('Пользователь не авторизован');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else if (res.data.error_code === 403) {
                // self.props.addMessageText('Нет доступа к запрашиваемому действию');
                self.props.addMessageText(res.data.text);
                openPopupSimple('messagePopup');
            } else {
                self.props.takeAnOrder(false);
            }
        })
    }

    returnInWork = () => {
        localStorage.setItem('paused', 'false');
        this.setState({
            paused: 'false'
        })
        this.findWork();
        this.props.takeAnOrder(false);
    }

    pausedWork = () => {
        this.pausedFunction();
    }

    render () {
        if (this.props.user.isLoggedIn === true) {
            if (this.state.paused === 'true') {
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            <div className="page">
                                <div className="search_work">
                                    <img src={require('../../images/icons/pause.svg')} width="101" height="101" alt=""/>
                                    <p>
                                        To resume the work, please click the button below.
                                    </p>
                                    <button className="btn btn_blue" onClick={this.returnInWork}>Continue work</button>
                                </div>
                            </div>
                        </main>
                    </div>
                )
            }
            if (this.props.get_order.isOrder === false) {
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            <div className="page">
                                <div className="search_work">
                                    <svg className="loader" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 534.9 536" enable-background="new 0 0 534.9 536">
                                        <circle fill="#29AEE3" cx="268" cy="59.9" r="59.9"/>
                                        <circle fill="#29AEE3" cx="415.1" cy="120.8" r="59.9"/>
                                        <circle fill="#29AEE3" cx="475" cy="268" r="59.9"/>
                                        <circle fill="#29AEE3" cx="415.1" cy="415.2" r="59.9"/>
                                        <circle fill="#29AEE3" cx="268" cy="476.1" r="59.9"/>
                                        <circle fill="#29AEE3" cx="120.8" cy="415.1" r="59.9"/>
                                        <circle fill="#29AEE3" cx="61" cy="268" r="59.9"/>
                                        <circle fill="#29AEE3" cx="120.8" cy="119.8" r="59.9"/>
                                    </svg>
                                    <p>
                                        We select a task for you, please stay on the page
                                    </p>
                                    <button className="btn btn_blue" onClick={this.pausedWork}>Suspend work</button>
                                </div>
                            </div>
                        </main>
                    </div>
                );
            } else {
                if (this.state.workList == null) {
                    this.props.takeAnOrder(false);
                    return false;
                }
                const {text_in, comment, price, language_in, language_out, type, category, type_text} = this.state.workList;
                const {text_out, wait_time, wait_input, notifications} = this.state;
                
                let sec_wait_time = wait_time % 60;
                let min_wait_time = Math.floor(wait_time / 60 % 60);
                let hours_wait_time = Math.floor(wait_time / 3600);
                sec_wait_time = sec_wait_time <= 9 ? '0' + sec_wait_time : sec_wait_time;
                min_wait_time = min_wait_time <= 9 ? '0' + min_wait_time : min_wait_time;
                hours_wait_time = hours_wait_time <= 9 ? '0' + hours_wait_time : hours_wait_time;
                const time_wait = hours_wait_time + ':' + min_wait_time + ':' + sec_wait_time;

                let second_wait_input = wait_input % 60;
                let min_wait_input = Math.floor(wait_input / 60 % 60);
                second_wait_input = second_wait_input <= 9 ? '0' + second_wait_input : second_wait_input;
                min_wait_input = min_wait_input <= 9 ? '0' + min_wait_input : min_wait_input;
                const input_wait = min_wait_input + ':' + second_wait_input;

                // const title = type === '1' ? 'Translate' : type === '2' ? 'Оценить качество' : 'Редактировать';
                const title = type_text;

                const titleComment = type === '1' || type === '2' ? 'Comment' : 'Correction';

                let riseUser = '';
                if (notifications != null) {
                    riseUser = notifications.list.map(item => (
                        <div id={'notifi_'+item.id} className='comment rise_comment'>
                            <div className="close" onClick={() => this.closeNotify(item.id, 'notifi_'+item.id)}>
                                <svg width="19" height="19" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                    <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59 c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                                </svg>
                            </div>
                            <div className="t">{item.title}</div>
                            <div className="text">{item.text}</div>
                        </div>
                    ))
                }

                const buttons = type === '2' ?
                    <div>
                        <div className="title_stars">Translation evaluation</div>
                        <div className="stars">
                            <i onClick={() => this.setStars(1)}></i>
                            <i onClick={() => this.setStars(2)}></i>
                            <i onClick={() => this.setStars(3)}></i>
                            <i onClick={() => this.setStars(4)}></i>
                            <i onClick={() => this.setStars(5)}></i>
                        </div>
                    </div> :
                    <button className="btn btn_blue" onClick={this.completeOrderFrom} disabled={!text_out} >{type === '1' ? 'Translated' : 'Corrected'}</button>
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            <div className="page work_interface">
                                <div className="title_page">{title}</div>
                                
                                { riseUser }

                                <div className={comment.length ? "comment" : 'hidden_comment'}>
                                    <div className="t">{titleComment}</div>
                                    <div className="text">{comment}</div>
                                </div>
                                <div className="info_order">
                                    <div className="col">
                                        <div className="item" data-tip="Amount of characters">{text_in.length}</div>
                                        <div className="item" data-tip="Income">${price}</div>
                                        <div className="item">
                                            <i className="ico artistic-brush" data-tip={category.name}></i>
                                            <div data-tip={language_in.name}>
                                                <img width="23" height="23" src={language_in.logo} alt={language_in.name}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="item" data-tip={language_out.name}>
                                            <img width="23" height="23" src={language_out.logo} alt={language_out.name}/>
                                        </div>
                                        <div className="item time_item">
                                            <i className="ico clock"></i>
                                            <div>
                                                <div data-tip="Time to translate">{time_wait}</div>
                                                <span data-tip="Time to start">{input_wait}</span>
                                            </div>
                                        </div>
                                        <div className="item button" onClick={this.pauseOrder} data-tip="Put on hold">
                                            <i className="ico pause"></i>
                                        </div>
                                        <div className="item button" onClick={this.nextOrder} data-tip="Next translation">
                                            <i className="ico next"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="text_translate">
                                    <div className="field">
                                        <TextareaAutosize value={text_in} readOnly disabled />
                                    </div>
                                    <div className="field">
                                        <TextareaAutosize value={text_out} onChange={this.handleText} readOnly={type === '2'} disabled={type === '2'} placeholder="Enter translation" />
                                    </div>
                                </div>
                                <div className="button_choose_wrapper">
                                    {buttons}
                                </div>
                            </div>
                            <ReactTooltip />
                        </main>
                        <CompleteForm completeOrder={this.completeOrder} price={price} />
                        <MessageForOrder cancelOrder={this.cancelOrder} />
                        {this.singlePush()}
                    </div>
                );
            }
        } else {
            return (
                <Redirect to={routes.HOME} />
            );
        }
    }
}

const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token,
    isOrder: state.get_order.isOrder
})

const mapDispatchToProps = dispatch => ({
    takeAnOrder: (isOrder) => dispatch(takeAnOrder(isOrder)),
    addMessageText: (text) => dispatch(addMessageText(text)),
    getCsrf: (csrf) => dispatch(getCsrf(csrf)),
    getCsrfAPI: axios.post(GET_CSRF_API).then(function (response) {
        dispatch(getCsrf(response.data.csrf))
    })
})

export default connect(mapStateToProps, mapDispatchToProps)(Work);