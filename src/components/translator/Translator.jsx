import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

import HeaderMain from '../partials/HeaderMain';
import AboutText from './partials/AboutText';
import Languages from './partials/Languages';
import Categories from './partials/Categories';

import { mapStateToProps, mapDispatchToProps } from '../../actions/get_orderAction';

class Translator extends Component {
    
    state = {
        isRedirect: false
    }

    componentDidMount () {
        if (this.props.user.isLoggedIn === false) {
            ReactGA.pageview(routes.TRANSLATOR);
        }
    }

    render () {
        if (this.props.user.isLoggedIn === false) {
            // if (this.state.isRedirect !== true) {
                
                const content = this.props.user.contentRegBlock ? <Languages /> : <Categories />
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            { content }
                            <AboutText />
                        </main>
                    </div>       
                );
            } else {
                return (
                    <Redirect to={routes.WORK} />
                )
            }
        // } else {
        //     return (
        //         // <div></div>
        //         <Redirect to={routes.HOME} />
        //     );
        // }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Translator);