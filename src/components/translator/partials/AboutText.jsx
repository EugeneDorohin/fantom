import React, { Component } from 'react';
import { connect } from 'react-redux';

class AboutText extends Component {

    
    render () {
        return (
            <div>
                <section className="how_it_work">
                    <div className="page">
                        <div className="title_inside">How it works</div>
                        <div className="steps">
                        <div className="step">
                            <div className="n">1</div>
                            <div className="text">Select languages</div>
                        </div>
                        <div className="step">
                            <div className="n">2</div>
                            <div className="text">Specify categories</div>
                        </div>
                        <div className="step">
                            <div className="n">3</div>
                            <div className="text">Register</div>
                        </div>
                        <div className="step">
                            <div className="n">4</div>
                            <div className="text">Make a translation</div>
                        </div>
                        <div className="step">
                            <div className="n">5</div>
                            <div className="text">Raise your level</div>
                        </div>
                        <div className="step">
                            <div className="n">6</div>
                            <div className="text">Pay out</div>
                        </div>
                    </div>
                    </div>
                </section>
                <div className="page">
                    <section className="why">
                        <div className="title_inside">Why it is cool</div>
                        <div className="items">
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/speedometer.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Speed</div>
                                    <p>
                                        Free registration for translators and, accordingly, their number create conditions for a quick response on each order, reducing speed to a minimum.
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/dollar-symbol.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Price</div>
                                    <p>
                                        The price depends on supply and demand, which creates a completely market situation and reduces the price to a possible minimum
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/quality.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Quality</div>
                                    <p>
                                        Your order will be performed by the best translator from the currently available ones. If you order the quality control, the best performers will check and edit the translation.
                                    </p>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ico"><img src={require('../../../images/icons/security.svg')} width="35" height="35" alt="" /></div>
                                <div className="text">
                                    <div className="t">Security</div>
                                    <p>
                                        If the received translation is incorrect, you can send it back for revision with a description and get corrected in a short time.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <section className="info_numbers">
                    <div className="page">
                        <div className="items">
                            <div className="item">
                                <div className="n">{this.props.staticData && this.props.staticData.total_promotions}</div>
                                <div className="text">Rises</div>
                            </div>
                            <div className="item">
                                <div className="n">{this.props.staticData && this.props.staticData.total_orders}</div>
                                <div className="text">Orders</div>
                            </div>
                            <div className="item">
                                <div className="n">$ {this.props.staticData && this.props.staticData.total_withdraws}</div>
                                <div className="text">Paid</div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="page">
                    <section className="our_mission">
                        <div className="title_inside">Our goal</div>
                        <div className="text">
                            Your order will be performed by the best translator from the currently available ones. If you order the quality control, the best performers will check and edit the translation.
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    staticData: state.staticData.staticData
})

export default connect(mapStateToProps)(AboutText);