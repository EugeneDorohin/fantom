import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closePopup } from '../../../../actions/form';

class CompleteForm extends Component {
    
    
    completeOrder = () => {
        this.props.completeOrder();
        closePopup();
    }

    render () {       
        return (
            <div className="popup_wrapper" data-popup="completeForm">
                <div className="bg" onClick={this.completeOrder}></div>
                <div className="popup">
                    <div className="content_popup">
                        <div className="message_content">
                            <div className="ico pay"></div>
                            <div className="text">
                                You earned ${this.props.price}
                            </div>
                            <button className="btn btn_blue pay" onClick={this.completeOrder}>Next translation</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
})

export default connect(mapStateToProps)(CompleteForm);