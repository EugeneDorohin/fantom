import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { mapDispatchToProps, mapStateToProps, mergeProps } from '../../../actions/languagesAction';

class Languages extends Component {
    state = {
        items: []
    }

    componentWillMount () {
        this.props.onChangeWithToken();
    }

    currentLang = (event) => {
        let target = event.currentTarget;
        
        if (target.classList.contains('current')) {
            this.setState({
                // items: this.state.items.filter(item => {
                //     return item.id !== target.getAttribute('data-key');
                // })
                items: this.state.items.filter(item => {
                    return item !== target.getAttribute('data-key');
                })
            });
            target.classList.remove('current');
        } else {
            if (document.querySelectorAll('.item_lang.current').length < 4) {
                // this.state.items.push({
                //     "id": target.getAttribute('data-key')
                // })
                this.state.items.push(target.getAttribute('data-key'));
                this.setState({items: this.state.items});
                target.classList.add('current');
            }
        }
    }

    addLanguages = () => {
        if (document.querySelectorAll('.item_lang.current').length >= 1) {
            window.scrollTo(0, 0);
            this.props.chooseLanguages(this.state.items);
            this.props.switchRegContent(false);
        }
    }

    render () {
        const itemsLanguages = this.props.languages && this.props.languages.map((lang, i) => (
            <div key={lang.id} data-key={lang.id} data-price={lang.price} className="item item_lang" onClick={this.currentLang}>
                <img className="image" src={lang.logo} alt={lang.name} width="38" height="38" />
                <div className="text">{lang.name}</div>
            </div>
        ));
        return (
            <div className="page wrapper_choose">
                <div className="title_page">I can translate</div>
                <div className="items_choose">
                    {itemsLanguages}
                </div>
                <div className="button_choose_wrapper">
                    <button className="btn btn_blue button_choose" onClick={this.addLanguages} disabled={this.state.items.length < 2}>Select</button>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Languages);