import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as routes from '../../../constants/routes';
import { mapDispatchToProps, mapStateToProps, mergeProps } from '../../../actions/categoriesAction';
import { openPopup } from '../../../actions/form';

class Categories extends Component {
    
    state = {
        items: []
    }
    
    componentWillMount () {
        this.props.onChangeWithToken()
    }

    currentCategories = (event) => {
        let target = event.currentTarget;
        
        if (target.classList.contains('current')) {
            this.setState({
                // items: this.state.items.filter(item => {
                //     return item.id !== target.getAttribute('data-key');
                // })
                items: this.state.items.filter(item => {
                    return item !== target.getAttribute('data-key');
                })
            });
            target.classList.remove('current');
        } else {
            // if (document.querySelectorAll('.item_category.current').length < 4) {
                // this.state.items.push({
                //     "id": target.getAttribute('data-key')
                // })
                this.state.items.push(target.getAttribute('data-key'))
                this.setState({items: this.state.items});
                target.classList.add('current');
            // }
        }
    }

    addCategories = () => {
        if (document.querySelectorAll('.item_category.current').length >= 1) {
            this.props.chooseCategories(this.state.items);
            openPopup('userForm');
        }
    }

    handleClick = () => {
        this.props.switchRegContent(true);
    }

    render () {
        const itemsCategories = this.props.categories.map(lang => (
            <div key={lang.id} data-key={lang.id} data-price={lang.price_mn} className="item item_category" onClick={this.currentCategories}>
                <img className="image" src={lang.logo} alt={lang.name} width="38" height="38" />
                <div className="text">{lang.name}</div>
            </div>
        ));
        return (
            <div className="page wrapper_choose">
                <div className="title_page">
                    <button className="back" onClick={this.handleClick}>
                        <svg width="38" height="38" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 400.004 400.004">
                            <path d="M382.688,182.686H59.116l77.209-77.214c6.764-6.76,6.764-17.726,0-24.485c-6.764-6.764-17.73-6.764-24.484,0L5.073,187.757 c-6.764,6.76-6.764,17.727,0,24.485l106.768,106.775c3.381,3.383,7.812,5.072,12.242,5.072c4.43,0,8.861-1.689,12.242-5.072 c6.764-6.76,6.764-17.726,0-24.484l-77.209-77.218h323.572c9.562,0,17.316-7.753,17.316-17.315 C400.004,190.438,392.251,182.686,382.688,182.686z"/>
                        </svg>
                    </button>
                    Categories
                </div>
                <div className="items_choose">
                    {itemsCategories}
                </div>
                <div className="button_choose_wrapper">
                    <button className="btn btn_blue button_choose" onClick={this.addCategories} disabled={!this.state.items.length}>Select</button>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Categories);