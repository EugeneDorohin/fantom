import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import * as routes from '../../constants/routes';
import ReactGA from 'react-ga';

import { NavLink } from 'react-router-dom';
import HeaderMain from '../partials/HeaderMain';
import axios from 'axios';
import { GET_HISTORY_API, GET_WITHDRAW_SYSTEM_API, GET_CSRF_API, GET_WITHDRAW_API, GET_HISTORY_TASK_API } from '../../constants/post_url';
import { getCsrf } from '../../actions/csrfAction';
import { getPayment, getHistory } from '../../actions/userAction';
import { addMessageText, openPopupSimple } from '../../actions/form';
import AddWithdraw from '../forms/AddWithdraw';
import { takeAnOrder } from '../../actions/get_orderAction';


// import { mapStateToProps, mapDispatchToProps } from '../../actions/get_orderAction';

class Balance extends Component {

    state = {
        id: '',
        name: '',
        paymentRequestReceived: false
    }

    componentWillMount () {
        this.props.takeAnOrder(true);
    }

    componentDidMount () {
        if (this.props.user.isLoggedIn === true) {
            ReactGA.pageview(routes.BALANCE);
        }
    }

    addWithdraw = (id, name) => {
        this.setState({
            id: id,
            name: name
        })
        openPopupSimple('addWithdraw');
    }

    paymentRequestReceived = () => {
        this.setState({
            paymentRequestReceived: true
        })
    }

    render () {
        if (this.props.user.isLoggedIn === true) {
            const { withdraw_systems, history_list } = this.props;
            const withdraw_systems_items = withdraw_systems && withdraw_systems.map(item => (
                <div className="item" onClick={() => this.addWithdraw(item.id, item.name)}>
                    <img src={item.logo} title={item.name} alt={item.name}/>
                </div>
            ));
            
            const history_list_title = history_list.length ? <div className="title_inside">History</div> : '';
            const history_list_items = history_list && history_list.map((item, index) => {
                let date = (item.date).split(' '),
                    time = date[1].split(':'),
                    hour = time[0],
                    minutes = time[1];
                return (
                    <div className="item">
                        <div className="">{date[0]}&nbsp;<i>{hour + ':' + minutes}</i></div>
                        <div className="">{item.type_text}</div>
                        <div className="">
                            { item.start_price != item.price ?
                                <span className="old">${ item.start_price }</span>
                                : ''
                            }
                            <span>${ item.price }</span>
                        </div>
                        <div className="">{item.status_text}</div>
                    </div>
                )
            });
            if (!this.state.paymentRequestReceived) {
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            <div className="balance_page page">
                                
                                <div className="score">
                                    <div>${this.props.user.userData.balance}</div>
                                    <span>${this.props.user.userData.wait_balance}</span>
                                </div>
    
                                <div className="withdraw_systems">
                                    { withdraw_systems_items }
                                </div>
    
                                <div className="history">
                                    { history_list_title }
                                    <div className="items">
                                        { history_list_items }
                                    </div>
                                </div>
                                
    
                            </div>
                        </main>
                        <AddWithdraw name={this.state.name} id={this.state.id} paymentRequestReceived={this.paymentRequestReceived} />
                    </div>       
                );
            } else {
                return (
                    <div className="wrapper_page">
                        <HeaderMain history={this.props.history} />
                        <main>
                            <div className="page">
                                <div className="info_block">
                                    <div className="ico"></div>
                                    <p>
                                        Payment will be made within 24 hours
                                    </p>
                                </div>

                                <div className="wrapper_button_work">
                                    <NavLink to={routes.WORK} className="btn">Translate again</NavLink>
                                </div>
                            </div>
                        </main>
                    </div>
                )
            }
        } else {
            return (
                <Redirect to={routes.HOME} />
            );
        }
    }
}

const mapStateToProps = state => ({
    ...state,
    csrf: state.csrf.token,
    withdraw_systems: state.user.withdraw_systems,
    history_list: state.user.history_list
});

export const mapDispatchToProps = dispatch => ({
    takeAnOrder: (isOrder) => dispatch(takeAnOrder(isOrder)),
    getCsrf: axios.post(GET_CSRF_API).then(function (res) {
        const getHistoryForm = new FormData();
        getHistoryForm.append('csrf', res.data.csrf);
        getHistoryForm.append('start', 0);
        getHistoryForm.append('length', 1000);
        axios({
            method: 'post',
            url: GET_HISTORY_TASK_API,
            data: getHistoryForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            dispatch(getCsrf(res.data.csrf));
            if (res.data.error_code === 201) {
                // dispatch(addMessageText('Пользователь не авторизован'));
                dispatch(addMessageText(res.data.text));
                openPopupSimple('messagePopup');
            } else {
                dispatch(getHistory(res.data.data.list));
            }
        });

        const listForm = new FormData();
        listForm.append('csrf', res.data.csrf);
        axios({
            method: 'post',
            url: GET_WITHDRAW_SYSTEM_API,
            data: listForm,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then(function (res) {
            dispatch(getCsrf(res.data.csrf));
            if (res.data.error_code === 201) {
                // dispatch(addMessageText('Пользователь не авторизован'));
                dispatch(addMessageText(res.data.text));
                openPopupSimple('messagePopup');
            } else {
                dispatch(getPayment(res.data.data));
            }
        })
    })
});

export default connect(mapStateToProps, mapDispatchToProps)(Balance);