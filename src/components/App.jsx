import '../scss/App.css';

import React, { Component } from 'react';
import axios from 'axios';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';

import ReactGA from 'react-ga';

import * as routes from '../constants/routes';
import Footer from './partials/Footer';

import About from './pages/About';
import Help from './pages/Help';
import Rules from './pages/Rules';
import Contact from './pages/Contact';
import Chat from './pages/Chat';

import Translate from './translate/Translate';
import Translator from './translator/Translator';
import Work from './translator/Work';
import Balance from './translator/Balance';

import Status from './translate/Status';
import FailPay from './translate/FailPay';

import NotFound from './pages/NotFound';

import Form from './forms';
import Message from './forms/Message';
import { mapStateToProps, mapDispatchToProps } from '../actions/userAction';
import { GET_CSRF_API } from '../constants/post_url';

class App extends Component {

    componentDidMount () {
        ReactGA.initialize('UA-129588630-1');
        const self = this;
        setInterval (()=>{
            axios.post(GET_CSRF_API).then(function (response) {
                self.props.getCsrf(response.data.csrf);
            });
        }, 1000 * 60 * 50);
    }

    render() {
        if (this.props.isLoad) {
            setTimeout(function () {
                document.getElementById('loader_page').classList.add('on');
            }, 150);
        }
        return (
            <BrowserRouter>
                <div id="all" className="load_page">

                    <Switch>
                        <Route exact path={routes.HOME} component={Translate} />

                        <Route exact path={routes.TRANSLATOR} component={Translator} />
                        <Route exact path={routes.WORK} component={Work} />
                        <Route exact path={routes.BALANCE} component={Balance} />

                        <Route exact path={routes.STATUS} component={Status} />
                        <Route exact path={routes.FAIL_PAY} component={FailPay} />
                        
                        <Route exact path={routes.ABOUT} component={About} />
                        <Route exact path={routes.HELP} component={Help} />
                        <Route exact path={routes.RULES} component={Rules} />
                        <Route exact path={routes.CONTACT} component={Contact} />
                        <Route exact path={routes.CHAT} component={Chat} />

                        <Route component={NotFound} />
                    </Switch>

                    <Footer />
                    <Form />
                    <Message />
                </div>
            </BrowserRouter>
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(App);
